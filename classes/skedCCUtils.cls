public with sharing class skedCCUtils {
	

    public static list<skedCCModels.PickListItem> getPicklistValues(string objName, String fld){
        list<skedCCModels.PickListItem> options = new list<skedCCModels.PickListItem>();
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values = getPicklistEntries(objName, fld);
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a : values)
        { 
            options.add(new skedCCModels.PickListItem(a.getValue(), a.getLabel() )); 
        }
        return options;
    }

    public static List<String> getStringPicklistValues(string objName, String fld) {
        List<String> retValues = new List<String>();
        // Get the list of picklist values for this field.
        list<Schema.PicklistEntry> values = getPicklistEntries(objName, fld);
        // Add these values to the values list.
        for (Schema.PicklistEntry a : values)
        { 
            retValues.add(a.getValue()); 
        }
        return retValues;
    }

    public static List<Schema.PicklistEntry> getPicklistEntries(String objName, String fld) {
        Schema.DescribeSObjectResult objDescribe = Schema.getGlobalDescribe().get(objName).getDescribe();       
        // Get a map of fields for the SObject
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        // Get the list of picklist values for this field.
        return fieldMap.get(fld).getDescribe().getPickListValues();
    }

    /*
        * seach object by name.
        * search by 'term%', if no records found then query by '%term%'.
    */
    public static skedResponse searchByName(String term, string objectName){
        skedResponse response = new skedResponse();        
        list<skedCCModels.PickListItem> result = new list<skedCCModels.PickListItem>();
        string searchTerm = '' + term + '%';
        //string searchTerm2 = '%' + term + '%';
        string searchQuery = 'select id, name from ' + objectName +' where name like:searchTerm limit 10';

        if(objectName == 'Contact'){
          searchQuery = 'select id, name, email from ' + objectName +' where name like:searchTerm limit 10';
        }
        if(objectName == 'Account'){
          searchQuery = 'select id, name, BillingStreet, BillingCity, BillingState ,BillingPostalCode, BillingCountry, Parent.name, Parent.id from ' 
            + objectName +' where name like:searchTerm limit 10';
        }

        try{
            List<sObject> sobjList = Database.query(searchQuery);
            //if search 'term%' not found, then search by '%term%'.
            if(sobjList.isEmpty()){
                searchTerm = '%' + term + '%';
                sobjList = Database.query(searchQuery);
            }
            for(sObject obj:sobjList){                
                result.add(new skedCCModels.PickListItem(obj));
            }    
            response.data = result;       
            response.success = true;
        } catch(Exception ex){
            response.getErrorMessage(ex);
        }        
        return response;
    }

    // Submit a record for approval
    public static boolean submitApproval(Id recordId, string processAPI){
        // submit approval.
        // Create an approval request for the account
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setComments('Submitting request by Apex Code.');
        request.setObjectId(recordId);

        // Submit on behalf of a specific submitter
        request.setSubmitterId(UserInfo.getUserId());        
        // Submit the record to specific process and skip the criteria evaluation
        request.setProcessDefinitionNameOrId(processAPI);
        request.setSkipEntryCriteria(true);                
        // Submit the approval request
        Approval.ProcessResult result = Approval.process(request);
        return result.isSuccess();
    }

    public static string CombineAddress(string street, string city, string state, string postalCode, string country) {
        string address = street;
        if (!string.isBlank(city)) {
            address += ', ' + city;
        }
        if (!string.isBlank(state)) {
            address += ', ' + state;
        }
        if (!string.isBlank(postalCode)) {
            address += ' ' + postalCode;
        }
        if (!string.isBlank(country)) {
            address += ', ' + country;
        }
        return address;
    }

    public static Map<string, Set<Date>> getHolidays() {
        Date currentDate = system.now().date().addDays(-1); //buffer for different timezone
        return skedUtils.getHolidays(currentDate);
    }

    public static String getObjectNameFromId(String strId) {
        if (String.isBlank(strId)) {
            return '';
        }

        Id objId = strId;
        return getObjectNameFromId(objId);
    }

    public static String getObjectNameFromId(Id objId) {
        Schema.SObjectType token = objId.getSObjectType();
        Schema.DescribeSObjectResult dr = token.getDescribe();
        return dr.Name.toUpperCase();
    }

    
    /**
    * @description
    * Get contentVersionId for latest Client Photo
    * @param contactIds
    * @return Map<Id, Id>: a map from clientId to contentVersionId
    */
    public static Map<Id, Id> getClientPhotoIds (Set<Id> contactIds) {
        Map<Id, Id> mpClientIdToContentVersionId = new Map<Id, Id>();

        if (contactIds.isEmpty()) {
            return mpClientIdToContentVersionId;
        }

        List<ContentDocumentLink> contLinks = [
            SELECT ContentDocumentId, ContentDocument.title, LinkedEntityId, ContentDocument.LatestPublishedVersionId
            FROM ContentDocumentLink
            WHERE ContentDocument.title = 'Client Photo' 
            AND LinkedEntityId IN :contactIds
        ];

        for (ContentDocumentLink cdl : contLinks) {
            if (cdl.ContentDocumentId != null && cdl.ContentDocument.LatestPublishedVersionId != null) {
                mpClientIdToContentVersionId.put(cdl.LinkedEntityId, cdl.ContentDocument.LatestPublishedVersionId);
            }
        }
        return mpClientIdToContentVersionId;
    }

    /**
    * @description
    * Get SF Photo URL from ContentDocumentId
    * @param contentVersionId
    * @return String: photo url
    */
    public static String getPhotoUrlFromContentVersionId(String contentVersionId) {
        // Get path from custom setting
        String urlPrefix = skedCCConstants.SF_CONTENTVERSION_URL;
        return urlPrefix + contentVersionId;
    }
}