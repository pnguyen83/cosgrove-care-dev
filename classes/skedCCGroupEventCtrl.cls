global class skedCCGroupEventCtrl {
	// The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public skedCCGroupEventCtrl(ApexPages.StandardController stdController) {
        // this.mysObject = (sObject)stdController.getRecord();
    }

    public skedCCGroupEventCtrl() {
        
    }

    @remoteAction
    global static skedResponse getConfigData() {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.getConfigData();
    }

    @remoteAction
    global static skedResponse searchByName(String term, string objectName){
        return skedCCUtils.searchByName(term, objectName);
    }

    @remoteAction
    global static skedResponse searchAccounts(String term){
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.searchAccount(term);
    }

    @remoteAction
    global static skedResponse getContactInfo(String contactId) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.getContactInfo(contactId);
    }

    @remoteAction
    global static skedResponse makeTeamLead(skedCCJobSchedulingModels.JobAllocationModel allocation) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.makeTeamLeader(allocation);
    }

    @remoteAction
    global static skedResponse removeTeamLead(skedCCJobSchedulingModels.JobAllocationModel allocation) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.removeTeamLeader(allocation);
    }

    @remoteAction
    global static skedResponse searchLocations(String term, String regionId){
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.searchLocation(term, regionId);
    }

    @remoteAction
    global static skedResponse searchAddresses(String searchingText) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.getAddresses(searchingText);
    }

    @remoteAction
    global static skedResponse getGeolocation(String address, String placeId) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.getGeolocation(address, placeId);
    }

    @remoteAction
    global static skedResponse searchCoordinators(String searchTerm) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.searchCoordinators(searchTerm);
    }

    @remoteAction
    global static skedResponse searchContacts(String searchTerm) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.searchContacts(searchTerm);
    }

    @remoteAction
    global static skedResponse saveGroupEvent(skedCCJobSchedulingModels.GroupEventModel groupEvent) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.saveGroupEvent(groupEvent);
    }

    @remoteAction
    global static skedResponse saveClient(String groupEventId, skedCCJobSchedulingModels.ClientServiceItemModel client) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.saveClient(groupEventId, client);
    }

    @remoteAction
    global static skedResponse removeClient(String clientId) {
       skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.removeClient(clientId);
    }

    @remoteAction
    global static skedResponse getGroupEventDetails(String groupEventId) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.getGroupEventDetails(groupEventId);
    }

    @remoteAction
    global static skedResponse saveJob(skedCCJobSchedulingModels.JobModel jobModel) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.saveJob(jobModel);
    }

    @remoteAction
    global static skedResponse cancelJob(String jobId, String reason) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.cancelJob(jobId, reason);
    }

    @remoteAction
    global static skedResponse getJobDetails(String jobId) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.getJobDetails(jobId);
    }

    @remoteAction
    global static skedResponse getAvailableResourcesForOffers(String jobId) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.getAvailableResourcesForOffers(jobId);
    }

    @remoteAction
    global static skedResponse createOffers(List<String> resourceIds, String jobId) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.createOffer(resourceIds, jobId);
    }

    @remoteAction
    global static skedResponse getAvailableResources(skedCCJobSchedulingModels.JobModel jobModel) {
        System.debug('jobModel = ' + jobModel);
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.getAvailableResources(jobModel);
    }

    @remoteAction
    global static skedResponse notifyResource(skedCCJobSchedulingModels.JobAllocationModel allocation) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.notifyResource(allocation.Id);
    }

    @remoteAction
    global static skedResponse removeResource(skedCCJobSchedulingModels.JobAllocationModel allocation, skedCCJobSchedulingModels.JobModel jobModel) {
        skedCCGroupEventCtrlHandler handler = new skedCCGroupEventCtrlHandler();
        return handler.removeAllocation(allocation, jobModel);
    }
}