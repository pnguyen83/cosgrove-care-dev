@isTest
public class skedCCDataSetUp {

    public static boolean skeduloAdminSettingsSetup = FALSE;

    public static void setupCustomSettings(){
        insert getCustomSettingsData();
    }

    public static List<sObject> getCustomSettingsData() {
        List<sObject> customSettings = new List<sObject>();

        customSettings.addAll(setupSkedConfigsSettings());
        customSettings.addAll(setupSkeduloShiftTypeSettings());

        return customSettings;
    }

    public static List<sObject> setupSkedConfigsSettings() {
        List<sObject> configs = new list<skedConfigs__c>();
        configs.add(new skedConfigs__c(Name='RM_JobStatusColors_Cancelled', Value__c='#df4040'));
        configs.add(new skedConfigs__c(Name='Global_SkeduloAPIToken', Value__c='abc'));
        configs.add(new skedConfigs__c(Name='Geocoding_Service', Value__c='Skedulo'));
        configs.add(new skedConfigs__c(Name='Global_GoogleAPIKEY', Value__c='abc'));
        configs.add(new skedConfigs__c(Name='RM_JOB_DETAILS', Value__c='sked__Start__c, sked__Finish__c, sked__Address__c'));
        configs.add(new skedConfigs__c(Name='RM_JOB_HOVER_FIELDS', Value__c='Name, sked__Type__c, sked__Duration__c, sked__Job_Status__c, sked__Start__c, sked__Finish__c, sked__Notes_Comments__c'));
        configs.add(new skedConfigs__c(Name='JobURL', Value__c='/'));
        configs.add(new skedConfigs__c(Name='RM_LocationOrSiteFilter', Value__c='Location'));
        configs.add(new skedConfigs__c(Name='RM_Schedule_Start', Value__c='630'));
        configs.add(new skedConfigs__c(Name='RM_Schedule_End', Value__c='2000'));
        configs.add(new skedConfigs__c(Name='RM_Schedule_Interval', Value__c='60'));
        configs.add(new skedConfigs__c(Name='Rest_Log_CustomEvent', Value__c='True'));
        configs.add(new skedConfigs__c(Name='Address_Type', Value__c='Location,Account'));
        configs.add(new skedConfigs__c(Name='Global_DefaultDeliveryMethod', Value__c='Scheduled Time'));
        configs.add(new skedConfigs__c(Name='Address_Country', Value__c='UK'));
        configs.add(new skedConfigs__c(Name='SF_ContentVersion_URL', Value__c='ABC'));
        

        return configs;
    }

    

    public static List<sObject> setupSkeduloShiftTypeSettings() {
        List<sObject> shiftTypeSettings = new List<sObject>();

        sked_Shift_Type_Setting__c setting1 = new sked_Shift_Type_Setting__c(
            Name = 'Annual Leave Setting',
            Shift_Type__c = 'Annual Leave',
            Can_Be_Pre_Dated__c = FALSE,
            Available_Start__c = 0,
            Available_End__c = 2400,
            Default_Start__c = NULL,
            Default_End__c = NULL,
            Step__c = 60,
            Not_Show__c = FALSE,
            Is_AVailable__c = FALSE,
            Color__c = '#D8D8D8'
        );
        shiftTypeSettings.add(setting1);

        sked_Shift_Type_Setting__c setting2 = new sked_Shift_Type_Setting__c(
            Name = 'Available Setting',
            Shift_Type__c = 'Available',
            Can_Be_Pre_Dated__c = FALSE,
            Available_Start__c = 0,
            Available_End__c = 2400,
            Default_Start__c = NULL,
            Default_End__c = NULL,
            Step__c = 30,
            Not_Show__c = FALSE,
            Is_AVailable__c = TRUE,
            Color__c = '#EBFFC6'
        );
        shiftTypeSettings.add(setting2);

        sked_Shift_Type_Setting__c setting3 = new sked_Shift_Type_Setting__c(
            Name = 'Occupied Setting',
            Shift_Type__c = 'Occupied Leave',
            Can_Be_Pre_Dated__c = FALSE,
            Available_Start__c = 0,
            Available_End__c = 2400,
            Default_Start__c = NULL,
            Default_End__c = NULL,
            Step__c = 15,
            Not_Show__c = FALSE,
            Is_AVailable__c = FALSE,
            Color__c = '#D8D8D8'
        );
        shiftTypeSettings.add(setting3);

        sked_Shift_Type_Setting__c setting4 = new sked_Shift_Type_Setting__c(
            Name = 'Sick Leave Setting',
            Shift_Type__c = 'Sick Leave',
            Can_Be_Pre_Dated__c = TRUE,
            Available_Start__c = 0,
            Available_End__c = 2400,
            Default_Start__c = NULL,
            Default_End__c = NULL,
            Step__c = 30,
            Not_Show__c = FALSE,
            Is_AVailable__c = FALSE,
            Color__c = '#D8D8D8'
        );
        shiftTypeSettings.add(setting4);

        return shiftTypeSettings;
    }

    

    public static Map<string, sObject> setupCommonTestData() {
        Map<string, sObject> mapTestData = new Map<string, sObject>();

        /*********************************************************1st Level**************************************************/
        List<sObject> firstList = new List<sObject>();
        /*********************************************************Region**************************************************/
        sked__Region__c regionQld = new sked__Region__c(
            Name = 'QLD',
            sked__Timezone__c = 'Australia/Queensland'
        );
        firstList.add(regionQld);
        mapTestData.put('regionQld', regionQld);

        sked__Region__c regionNsw = new sked__Region__c(
            Name = 'NSW',
            sked__Timezone__c = 'Australia/NSW'
        );
        firstList.add(regionNsw);

        sked__Region__c regionVic = new sked__Region__c(
            Name = 'VIC',
            sked__Timezone__c = 'Australia/Victoria'
        );
        firstList.add(regionVic);

        /*********************************************************Tag**************************************************/
        sked__Tag__c tagQualityControl = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        firstList.add(tagQualityControl);
        mapTestData.put('tagQualityControl', tagQualityControl);

        sked__Tag__c tagServerEngineer = new sked__Tag__c(
            Name = 'Server Engineer',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        firstList.add(tagServerEngineer);
        mapTestData.put('tagServerEngineer', tagServerEngineer);

        /*********************************************************Holidays**************************************************/
        sked__Holiday__c globalHoliday = new sked__Holiday__c(
            Name = 'Global Holidays',
            sked__Start_Date__c = system.today().addDays(1),
            sked__End_Date__c = system.today().addDays(1),
            sked__Global__c = true
        );
        firstList.add(globalHoliday);

        sked__Holiday__c qldHoliday = new sked__Holiday__c(
            Name = 'Qld Holidays',
            sked__Start_Date__c = system.today().addDays(1),
            sked__End_Date__c = system.today().addDays(1),
            sked__Global__c = false
        );
        firstList.add(qldHoliday);
        /*********************************************************Account**************************************************/
        Account accountTest = new Account(
            Name = 'Test Account',
            BillingCountry = 'Australia',
            BillingStreet = '12 Maynard Rd',
            BillingCity = 'Plympton Park',
            BillingState = 'South Australia',
            BillingPostalCode = '5038'
        );
        //firstList.add(accountTest);
        mapTestData.put('accountTest', accountTest);

        /*********************************************************CC Group**************************************************/
        

        sked__Availability_Template__c availabilityTemplate = new sked__Availability_Template__c(
            sked__Global__c = false
        );

        firstList.add(availabilityTemplate);

        // Enrite service
        insert firstList;
        /*********************************************************End of 1st level**************************************************/

        /*********************************************************2nd Level**************************************************/
        List<sObject> secondList = new List<sObject>();
        /*********************************************************Region holidays**************************************************/
        sked__Holiday_Region__c qldRegionHoliday = new sked__Holiday_Region__c(
            sked__Holiday__c = qldHoliday.Id,
            sked__Region__c = regionQld.Id
        );
        secondList.add(qldRegionHoliday);
        /*********************************************************Contact**************************************************/
        Contact contactTest = new Contact(
            FirstName = 'Test',
            LastName = 'Contact',
            AccountId = accountTest.Id,
            MailingCountry = 'Australia',
            MailingStreet = '12 Maynard Rd',
            MailingCity = 'Plympton Park',
            MailingState = 'South Australia',
            MailingPostalCode = '5038'
        );
        secondList.add(contactTest);
        mapTestData.put('contactTest', contactTest);

        Contact contactTest2 = new Contact(
                FirstName = 'Test',
                LastName = 'Contact2',
                AccountId = accountTest.Id
        );
        secondList.add(contactTest2);
        mapTestData.put('contactTest2', contactTest2);

        Contact employeeTest = new Contact(
                FirstName = 'Test',
                LastName = 'Employee',
                AccountId = accountTest.Id,
                RecordTypeId=Contact.SObjectType.getDescribe().getRecordTypeInfosByName().get('Resource').recordTypeId
        );

        secondList.add(employeeTest);
        mapTestData.put('employeeTest', employeeTest);

        Contact clientTest = new Contact(
                FirstName = 'Test',
                LastName = 'Client',
                AccountId = accountTest.Id,
                Phone = '1234567890',
                Email = 'test@test.com',
                RecordTypeId=Contact.SObjectType.getDescribe().getRecordTypeInfosByName().get('Supplier').recordTypeId
        );

        secondList.add(clientTest);
        mapTestData.put('clientTest', clientTest);
        /*********************************************************Opportunity**************************************************/
        Opportunity opportunityTest = new Opportunity(
            Name = 'Test Opportunity',
            AccountId = accountTest.Id,
            StageName = 'Negotiation',
            CloseDate = system.today().addMonths(1)
        );
        secondList.add(opportunityTest);
        mapTestData.put('opportunityTest', opportunityTest);

        /*********************************************************Resource**************************************************/
        sked__Resource__c resourceTeamLeader = new sked__Resource__c(
            Name = 'Team Leader',
            sked__Primary_Region__c = regionQld.Id,
            sked__Category__c = 'Team Leader',
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            sked__User__c = UserInfo.getUserId(),
            sked__Is_Active__c = TRUE,
            sked__Email__c = 'abc@com.pho'
        );
        secondList.add(resourceTeamLeader);
        mapTestData.put('resourceTeamLeader', resourceTeamLeader);

        sked__Resource__c resourceCustomerService1 = new sked__Resource__c(
            Name = 'Customer Service 1',
            sked__Primary_Region__c = regionQld.Id,
            sked__Category__c = 'Customer Service',
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            sked__Is_Active__c = TRUE
        );
        secondList.add(resourceCustomerService1);
        mapTestData.put('resourceCustomerService1', resourceCustomerService1);

        sked__Resource__c resourceCustomerService2 = new sked__Resource__c(
            Name = 'Customer Service 2',
            sked__Primary_Region__c = regionQld.Id,
            sked__Category__c = 'Customer Service',
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            sked__Is_Active__c = TRUE
        );
        secondList.add(resourceCustomerService2);
        mapTestData.put('resourceCustomerService2', resourceCustomerService2);

        sked__Resource__c resourceCustomerService3 = new sked__Resource__c(
            Name = 'Customer Service 3',
            sked__Primary_Region__c = regionQld.Id,
            sked__Category__c = 'Customer Service',
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            sked__Is_Active__c = TRUE
        );
        secondList.add(resourceCustomerService3);
        mapTestData.put('resourceCustomerService3', resourceCustomerService3);

        sked__Location__c location = new sked__Location__c(
            Name = 'Test Location',
            sked__Region__c = regionQld.Id
        );
        mapTestData.put('location', location);
        secondList.add(location);

        
        // Availability template
        secondList.add(createAvailabilityTemplateEntryData(availabilityTemplate.Id, 'MON'));
        secondList.add(createAvailabilityTemplateEntryData(availabilityTemplate.Id, 'TUE'));
        secondList.add(createAvailabilityTemplateEntryData(availabilityTemplate.Id, 'WED'));
        secondList.add(createAvailabilityTemplateEntryData(availabilityTemplate.Id, 'THU'));
        secondList.add(createAvailabilityTemplateEntryData(availabilityTemplate.Id, 'FRI'));
        secondList.add(createAvailabilityTemplateEntryData(availabilityTemplate.Id, 'SAT'));
        secondList.add(createAvailabilityTemplateEntryData(availabilityTemplate.Id, 'SUN'));

        insert secondList;
        /*********************************************************End of 2nd level**************************************************/

        /*********************************************************3rd Level**************************************************/
        List<sObject> thirdList = new List<sObject>();
        /*********************************************************CustomSetting**************************************************/
        thirdList.addAll(getCustomSettingsData());

        /*********************************************************Service Delivery**************************************************/


        /*********************************************************Resource Tags**************************************************/
        sked__Resource_Tag__c customerService1_qualityControl = new sked__Resource_Tag__c(
            sked__Resource__c = resourceCustomerService1.Id,
            sked__Tag__c = tagQualityControl.Id,
            Active__c = TRUE
        );
        thirdList.add(customerService1_qualityControl);

        sked__Resource_Tag__c customerService1_serverEngineer = new sked__Resource_Tag__c(
            sked__Resource__c = resourceCustomerService1.Id,
            sked__Tag__c = tagServerEngineer.Id,
            Active__c = TRUE
        );
        thirdList.add(customerService1_serverEngineer);

        sked__Resource_Tag__c customerService2_serverEngineer = new sked__Resource_Tag__c(
            sked__Resource__c = resourceCustomerService2.Id,
            sked__Tag__c = tagServerEngineer.Id,
            Active__c = TRUE
        );
        thirdList.add(customerService2_serverEngineer);

        sked__Resource_Tag__c customerService2_qualityControl = new sked__Resource_Tag__c(
            sked__Resource__c = resourceCustomerService2.Id,
            sked__Tag__c = tagQualityControl.Id,
            Active__c = TRUE
        );
        thirdList.add(customerService2_qualityControl);

        sked__Resource_Tag__c resLead_serverEngineer = new sked__Resource_Tag__c(
            sked__Resource__c = resourceTeamLeader.Id,
            sked__Tag__c = tagServerEngineer.Id,
            Active__c = TRUE
        );
        thirdList.add(resLead_serverEngineer);

        Client_Tag__c contactTest_qualityControl = new Client_Tag__c(
            Client__c = contactTest.Id,
            Tag__c = tagQualityControl.Id,
            Required__c = true
        );

        thirdList.add(contactTest_qualityControl);

        thirdList.add(createAvailabilityTemplateResourceData(resourceTeamLeader.Id, availabilityTemplate.Id));
        thirdList.add(createAvailabilityTemplateResourceData(resourceCustomerService1.Id, availabilityTemplate.Id));
        thirdList.add(createAvailabilityTemplateResourceData(resourceCustomerService2.Id, availabilityTemplate.Id));
        thirdList.add(createAvailabilityTemplateResourceData(resourceCustomerService3.Id, availabilityTemplate.Id));

        insert thirdList;
        /*********************************************************End of 3rd level**************************************************/

        return mapTestData;
    }

    //*********************************************************Create Enrite service data*********************************************************
    public static Map<String, RecordType> RecordTypes
    {
        get
        {
            if (RecordTypes == null)
            {
                RecordTypes = new Map<String, RecordType>();

                for (RecordType rt : [SELECT Id,
                        sObjectType,
                        DeveloperName
                FROM RecordType])
                {
                    RecordTypes.put(rt.sObjectType + ':' + rt.DeveloperName, rt);
                }
            }
            return RecordTypes;
        }
        set;
    }

    

    public static Map<string, sObject> setupSDJMTestData() {
        Map<string, sObject> mapTestData = setupCommonTestData();

        List<sked__Resource__c> resources = new List<sked__Resource__c>();
        resources.add((sked__Resource__c)mapTestData.get('resourceTeamLeader'));
        resources.add((sked__Resource__c)mapTestData.get('resourceCustomerService1'));
        resources.add((sked__Resource__c)mapTestData.get('resourceCustomerService2'));
        resources.add((sked__Resource__c)mapTestData.get('resourceCustomerService3'));

        DateTime currentTime = system.now();
        DateTime tempDt = currentTime;
        List<sked__Availability__c> resourceAvailabilities = new List<sked__Availability__c>();
        for (sked__Resource__c resource : resources) {
            while (tempDt < currentTime.addMonths(1)) {
                DateTime startOfDay = DateTime.newInstance(tempDt.date(), time.newInstance(0, 0, 0, 0));
                DateTime endOfDay = startOfDay.addDays(1);
                sked__Availability__c avail = new sked__Availability__c(
                    sked__Resource__c = resource.Id,
                    sked__Is_Available__c = TRUE,
                    sked__Type__c = 'Available',
                    sked__Status__c = 'Approved',
                    sked__Start__c = startOfDay,
                    sked__Finish__c = endOfDay
                );
                resourceAvailabilities.add(avail);

                tempDt = tempDt.addDays(1);
            }
        }
        insert resourceAvailabilities;

        return mapTestData;
    }

    public static Map<string, sObject> setupJobTestData() {
        setupSkeduloShiftTypeSettings();
        Map<string, sObject> mapTestData = setupCommonTestData();
        sked__Region__c regionQld = (sked__Region__c)mapTestData.get('regionQld');
        DateTime dt = system.now();
        List<sked__Job__c> jobList = new List<sked__Job__c>();

        for(integer i = 1; i <= 50; i++) {
            string durationText = Math.mod(i, 5) == 1 ? '1h' : Math.mod(i, 5) + 'd';
            sked__Job__c job = new sked__Job__c(

                sked__Type__c = 'Desktop',
                sked__Region__c = regionQld.Id,
                sked__Start__c = dt,
                sked__Finish__c = dt.addHours(1),
                sked__Duration__c = 60,
                sked__Address__c = '79 mclachlan Fortitude valley',
                sked__Job_Status__c = 'Pending Dispatch'
            );
            jobList.add(job);
            mapTestData.put('job' + i, job);
            if(Math.mod(i, 5) == 0) {
                dt = dt.addDays(1);
            }
        }
        insert jobList;

        sked__Job__c parentJob = jobList.get(5);
        sked__Job__c followJob1 = jobList.get(6);
        followJob1.sked__Parent__c = parentJob.Id;
        sked__Job__c followJob2 = jobList.get(6);
        followJob2.sked__Parent__c = parentJob.Id;

        return mapTestData;
    }

    public static sked__Availability_Template_Entry__c createAvailabilityTemplateEntryData(Id templateId, String weekDay){
        sked__Availability_Template_Entry__c newAvailabilityTemplateEntry = new sked__Availability_Template_Entry__c(
            sked__Availability_Template__c = templateId,
            sked__Finish_Time__c = 2400,
            sked__Is_Available__c = true,
            sked__Start_Time__c = 0,
            sked__Weekday__c = weekDay
        );
        return newAvailabilityTemplateEntry;
    }

    public static sked__Availability_Template_Resource__c createAvailabilityTemplateResourceData(Id resID, Id tempID){
        sked__Availability_Template_Resource__c newAvailabilityTemplateResource = new sked__Availability_Template_Resource__c(
            sked__Availability_Template__c = tempId,
            sked__Resource__c = resId
        );

        return newAvailabilityTemplateResource;
    }

}