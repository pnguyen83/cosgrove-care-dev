@isTest
public class skedTestDataFactory {
    
    /*
    *
    */
    public static void setupCustomSettings(){
        list<skedConfigs__c> configs = new list<skedConfigs__c>();
        configs.add(new skedConfigs__c(Name='RM_JobStatusColors_Cancelled', Value__c='#df4040'));
        configs.add(new skedConfigs__c(Name='Global_SkeduloAPIToken', Value__c='abc'));
        configs.add(new skedConfigs__c(Name='Geocoding_Service', Value__c='Skedulo'));
        configs.add(new skedConfigs__c(Name='Global_GoogleAPIKEY', Value__c='abc'));
        configs.add(new skedConfigs__c(Name='RM_JOB_DETAILS', Value__c='sked__Start__c, sked__Finish__c, sked__Address__c'));
        configs.add(new skedConfigs__c(Name='RM_JOB_HOVER_FIELDS', Value__c='Name, sked__Type__c, sked__Duration__c, sked__Job_Status__c, sked__Start__c, sked__Finish__c, sked__Notes_Comments__c'));
        configs.add(new skedConfigs__c(Name='JobURL', Value__c='/'));
        configs.add(new skedConfigs__c(Name='RM_LocationOrSiteFilter', Value__c='Location'));
        configs.add(new skedConfigs__c(Name='RM_Schedule_Start', Value__c='630'));
        configs.add(new skedConfigs__c(Name='RM_Schedule_End', Value__c='2000'));
        configs.add(new skedConfigs__c(Name='RM_Schedule_Interval', Value__c='60'));
        configs.add(new skedConfigs__c(Name='Service_Delivered_Date', Value__c='Job Allocation'));

        insert configs;

    }
    /*
    * Initializes a list of Account
    */
    public static List<Account> createAccounts(String basename, String rtName, Integer count){
        List<Account> records = new List<Account>();
        for( Integer i=0; i< count; i++ ){
            Account reccord = new Account(   
                                            RecordTypeId    = getRecordTypeID(Schema.sObjectType.Account, rtName),
                                            Name            = basename + String.valueOf(i),
                                            ShippingStreet  = '79 McLachlan St Fortitude Valley'
                                        ) ;
            
            records.add( reccord);
        }
        return records;
    }

    /*
    * Get Record Type ID by Name
    */
    public static Id getRecordTypeId(Schema.DescribeSObjectResult d, String name){
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        if(rtMapByName.containsKey(name)){
            return rtMapByName.get(name).getRecordTypeId();
        }
        return null;
    }


    /*
    * Initializes a list of Job
    */
    public static List<sked__Job__c> createJobs(Id accountId, Id regionId, Id groupEventId, Integer count){
        List<sked__Job__c> records = new List<sked__Job__c>();
        for( Integer i=0; i< count; i++ ){
            sked__Job__c reccord = new sked__Job__c( 
                                            sked__Account__c        = accountId,
                                            sked__Start__c          = System.now().addHours(9),
                                            sked__Finish__c         = System.now().addHours(10),
                                            sked__Duration__c       = 60,
                                            sked__Region__c         = regionId,
                                            Group_Event__c          = groupEventId,
                                            sked__Address__c        = '79 mclachlan'
                                        ) ;
            
            records.add( reccord);
        }
        return records;
    }

    /*
    * Allocate a job to a resource
    */
    public static sked__Job_Allocation__c allocateJob(Id jobId, Id resourceId){
        return new sked__Job_Allocation__c(
                sked__Job__c            = jobId,
                sked__Resource__c       = resourceId,
                sked__Assigned_To__c    = resourceId,
                sked__Time_Start_Travel__c   = System.now().addHours(-2),
                sked__Time_In_Progress__c    = System.now().addHours(-1),
                sked__Time_Completed__c    = System.now()
            );
    }

   /*
    * Initializes a Region
    */
    public static sked__Region__c createRegion(String name, String timezone){
        
        sked__Region__c region = new sked__Region__c(
            Name = name,
            sked__Timezone__c = timezone==null?'Australia/Melbourne':timezone,
            sked__Country_Code__c = 'AU'
        );
            
        return region;
    }

    /*
    * Initializes a Location
    */
    public static sked__Location__c createLocation(String name, Id accountId, Id regionId){
        
        sked__Location__c location = new sked__Location__c(
            Name                = name,
            sked__Account__c    = accountId,
            sked__Region__c     = regionId
        );
            
        return location;
    }
	
    /*
    * Initializes a Contact
    */
    public static Contact createContact(Id accountId, String name, String recordType, Id regionId){
        return new Contact(
                FirstName       = name,
                LastName        = name,
                AccountId       = accountId, 
                RecordTypeId    = skedTestDataFactory.getRecordTypeIDByDeveloperName('Contact',recordType),
                sked__Region__c = regionId,
                Birthdate       = System.today().addDays(-10000),
                otherStreet     = 'Test',
                otherCity       = 'Test',
                otherState      = 'Test',
                otherPostalCode = 'Test',
                otherCountry    = 'Test',
                Phone           = '0111111111',
                Email           = 'test@test.com'
            );
    }

    /*
    * Initializes a Tag
    */
    public static sked__Tag__c createTag(String name){
        
        return new sked__Tag__c(
                name            = name,
                sked__Type__c   = 'Skill',
                sked__Classification__c = 'Global'
            );
    }

    /*
    * Initializes a Job Tag
    */
    public static sked__Job_Tag__c createJobTag(Id jobId, Id tagId){
        
        return new sked__Job_Tag__c(
                sked__Job__c   = jobId,
                sked__Tag__c = tagId
            );
    }

    /*
    * Initializes a Client Tag
    */
    public static Client_Tag__c createClientTag(Id clientId, Id tagId){
        
        return new Client_Tag__c(
                Client__c   = clientId,
                Tag__c = tagId,
                Required__c = true
            );
    }

    /*
    * Initializes a Resource
    */
    public static sked__Resource__c createResource(String name, Id userId, Id regionId){
        return new sked__Resource__c(
                name                    = name,
                sked__Resource_Type__c  = 'Person',
                sked__Primary_Region__c = regionId,
                sked__Category__c       = 'Customer Service',
                sked__Country_Code__c   = 'AU',
                sked__Home_Address__c   = '24 Tuckett Rd, Salisbury, Queensland, AUS',
                sked__Is_Active__c      = true,
                sked__Weekly_Hours__c   = 40,
                sked__User__c           = userId
            );
    }

    /*
    * Initializes an Availability record
    */
    public static sked__Availability__c createAvailability(Id resourceId, boolean isAvailable, String timezone){
        
        return new sked__Availability__c(
                sked__Resource__c       = resourceId,
                sked__Timezone__c       = timezone==null?'Australia/Brisbane':timezone,
                sked__Is_Available__c   = isAvailable,
                sked__Type__c           = 'Available',
                sked__Status__c         = 'Approved',
                sked__Start__c          = System.now(),
                sked__Finish__c         = System.now().addDays(1)
            );
    }


    public static Group_Event__c createGroupEvent(Id contactId, Id regionId, Id accountId){
        return new Group_Event__c(
                Name                = 'Test Group',
                Group_Status__c     = 'Active',
                Coordinator__c      = contactId,
                Address__c          = 'Test Address',
                Region__c           = regionId,
                Account__c          = accountId
            );
    }

    public static Group_Client__c createGroupClient(Id groupEventId, Id clientId, Id saID, Id saiID){
        return new Group_Client__c(
                Group_Event__c  = groupEventId,
                Client__c       = clientId
            );
    }

    
    /*
    * Get object's record type developer names
    * - objectAPIName
    * - developerNames: All - get all developer names
    *                   Otherwise: a list of developer names, seperated by commas
    */
    public static String getRecordTypeIDByDeveloperName(String objectAPIName, String developerName){
        if(String.isBlank(developerName)) return '';
        //Get IDs by developer names
        for(RecordType rt : [select Id, Name, DeveloperName from RecordType where sobjecttype=:objectAPIName]){
            if(rt.DeveloperName == developerName) return rt.Id;
        }
        return null;
    }
}