@isTest
public class skedUtilsTest {
	testmethod static void doTest() {
		Date fromDate = System.today();
		Date toDate = fromDate.addDays(2);
		String timezone = UserInfo.getTimeZone().getId();
		String fromDateISO = skedUtils.ConvertDateToIsoString(fromDate);
		String toDateISO = skedUtils.ConvertDateToIsoString(toDate);
		DateTime now = System.now();
		String GMTTimezone = 'GMT+7';

		Test.startTest();
  		skedUtils.GetDifferentDays(fromDateISO, toDateISO);
  		skedUtils.GetStartOfDate(fromDateISO, timezone);
  		skedUtils.GetStartOfDate(now, timezone);
  		skedUtils.GetEndOfDate(now, timezone);
  		skedUtils.convertToTimezone(now, GMTTimezone);
  		skedUtils.addDays(now, 2, timezone);
  		skedUtils.ConvertToDateValue('2018-04-02');
  		skedUtils.ConvertBetweenTimezones(now, GMTTimezone, timezone);
  		skedUtils.ConvertTimeNumberToMinutes(100);
  		skedUtils.daySequenceOfWeek('Mon');
  		skedUtils.daySequenceOfWeek('Tue');
  		skedUtils.daySequenceOfWeek('Wed');
  		skedUtils.daySequenceOfWeek('Thu');
  		skedUtils.daySequenceOfWeek('Fri');
  		skedUtils.daySequenceOfWeek('Sat');
  		skedUtils.daySequenceOfWeek('Sun');
  		skedUtils.addMinutes(now, 10, GMTTimezone);
  		skedUtils.increaseDays(10, true, now, GMTTimezone, false, false, new Set<Date>());
  		skedUtils.getValidDay(10,4,1983);
  		skedUtils.getTime('9:30');
  		skedUtils.addYears(now, 2, GMTTimezone);
		Test.stopTest();
	}
}