public class skedCCGroupEventCtrlHandler extends skedCCBaseEventCtrl{
	public skedResponse searchCoordinators(String searchTerm) {
        skedResponse response = new skedResponse();
        try {
            List<CordinatorModel> cords = searchCoordinator(searchTerm);

            response.success = true;
            response.data = cords;     
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse getAvailableResourcesForOffers(String jobId) {
        List<sked__Job__c> jobs = [SELECT Id, sked__Start__c, sked__Finish__c FROM sked__Job__c WHERE Id = :jobId];
        if (jobs == null || jobs.isEmpty() || jobs.get(0).sked__Start__c == null || jobs.get(0).sked__Finish__c == null) {
            skedResponse response = new skedResponse();
            response.errorMessage = 'Attention: Job is missing job time';
            return response;
        }
        return checkAvaiResourceForOffer(jobId, true);
    }

    public skedResponse createOffer(List<String> lstResIds, String jobID) {
        skedResponse response = new skedResponse();

        try {
            createShiftOffers(lstResIds, new List<String>{jobId}); 
            response.success = true;   
        }
        catch (Exception ex) {
            response.getErrorMessage(ex);
        }
        
        return response;
    }

    public skedResponse makeTeamLeader(skedCCJobSchedulingModels.JobAllocationModel allocation) {
        String jaID = allocation.id;
        skedResponse response = new skedResponse();
        response = setTeamLeader(jaID, true);
        return response;

    }

    public skedResponse removeTeamLeader(skedCCJobSchedulingModels.JobAllocationModel allocation) {
        String jaID = allocation.id;
        skedResponse response = new skedResponse();
        response = setTeamLeader(jaID, false);
        return response;
    }

    public skedResponse searchContacts(String searchTerm) {
        skedResponse response = new skedResponse();
        try {
            System.debug('searchTerm = ' + searchTerm);
            List<ClientModel> clients = searchClients(searchTerm);

            response.success = true;
            response.data = clients;     
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse saveGroupEvent(GroupEventModel groupEvent) {
        skedResponse response = new skedResponse();
        SavePoint sp = Database.setSavepoint();

        try {
            if (String.isBlank(groupEvent.id)) {
                handleGroupEventInsert(groupEvent);
            } else {
                handleGroupEventUpdate(groupEvent);
            }
            response.success = true;
            response.data = groupEvent.id;
        } catch ( Exception ex ) {
            Database.rollback(sp);
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse saveClient(String groupEventId, ClientServiceItemModel client) {
        skedResponse response = new skedResponse();
        SavePoint sp = Database.setSavepoint();

        try {
            saveGroupClient(groupEventId, client);
            response.success = true;
            response.data = client.id;
        } catch ( Exception ex ) {
            Database.rollback(sp);
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse removeClient(String groupClientId) {
        skedResponse response = new skedResponse();
        SavePoint sp = Database.setSavepoint();
        try {
            removeGroupClient(groupClientId);
            response.success = true;
        } catch ( Exception ex ) {
            Database.rollback(sp);
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse saveJob(JobModel jobModel) {
        skedResponse response = new skedResponse();

        SavePoint sp = Database.setSavepoint();
        JobSavingModel savedJobModel = new JobSavingModel();
        try {
            if (String.isBlank(jobModel.id)) {
                handleJobInsert(jobModel);
            } else {
                handleJobUpdate(jobModel);
            }

            response.success = true;
            savedJobModel.jobId = jobModel.id;
            response.data = savedJobModel;
        } catch ( Exception ex ) {
            System.debug('aaa error: ' + ex.getMessage() + ', at line: ' + ex.getStackTraceString());
            Database.rollback(sp);
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse cancelJob(String jobId, String reason) {
        skedResponse response = new skedResponse();

        SavePoint sp = Database.setSavepoint();
        try {
            response.success = true;
            cancelJobAction(jobId, reason, response);
        } catch ( Exception ex ) {
            Database.rollback(sp);
            response.getErrorMessage(ex);
        }
        return response;
    }

   	public skedResponse getGroupEventDetails(String groupEventId) {
        skedResponse response = new skedResponse();

        try {
            response.success = true;
            response.data = buildGroupEventModel(groupEventId);
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }


// ------------------------------Private functions--------------------------------------------------------------------------------------------------------------------------------------
    private List<ClientModel> searchClients(String searchTerm) {
        List<ClientModel> lstClients = new List<ClientModel>();
        String strSearch = '%' + searchTerm + '%';
        strSearch = strSearch.replace(' ', '%');
        Map<String, Contact> mapContacts = new Map<String, Contact>();
        Map<String, String> mapContactPictures = new Map<String, String>();
        
        for (Contact ct : [SELECT Id, Name, Birthdate, MobilePhone, PhotoUrl, Account.Name, MailingAddress, Mailing_Address_Latitude__c, Mailing_Address_Longitude__c,
                                    (select Id, Client__c, Tag__c, Tag__r.Name, Weighting__c, Required__c FROM Client_Tags__r)
                            FROM Contact WHERE Name LIKE : strSearch AND RecordType.Name = :skedCCConstants.CONTACT_RECORD_TYPE_SUPPORTED_INDIVIDUAL]) {
            System.debug('ct = ' + ct);
            mapContacts.put(ct.id, ct);
        }
        
        mapContactPictures = getContactPhoto(mapContacts.keySet());
        
        for (String contactId : mapContacts.keySet()) {
            Contact ct = mapContacts.get(contactId); 
            ClientModel cm = new ClientModel(ct);
            cm.photoUrl = mapContactPictures.get(contactId);
            cm.address = new JobLocation();

            if(ct.MailingAddress!=null){
                cm.address.fullAddress = skedCCUtils.combineAddress( ct.MailingAddress.getStreet(), ct.MailingAddress.getCity(), 
                                                                        ct.MailingAddress.getState(), ct.MailingAddress.getPostalCode(), 
                                                                        ct.MailingAddress.getCountry() );
                cm.address.latitude = ct.MailingAddress.getLatitude();
                cm.address.longitude = ct.MailingAddress.getLongitude();
            }

            System.debug('cm = ' + cm);
            lstClients.add(cm);
            
        }

        return lstClients;
    }

    private void saveGroupClient(String groupEventId, ClientServiceItemModel client) {
        if (String.isBlank(groupEventId)) {
            throw new skedCoreException('Group Event Id not found!!!');
        }

        if (client == null) {
            throw new skedCoreException('Client Data not found!!!');
        }

        Group_Client__c groupClient  = newGroupClientFrom(groupEventId, client);
        if (String.isNotBlank(client.Id)) {
            groupClient.Id = client.Id;
        }

        upsert groupClient;
        client.Id = groupClient.Id;
    }

    private void removeGroupClient(String groupClientId) {
        List<Group_Client__c> groupClients = [
            SELECT Id, Name FROM Group_Client__c WHERE Id = :groupClientId
        ];

        if (!groupClients.isEmpty()) {
            delete groupClients[0];
        }
    }

    private void insertGroupClients(Group_Event__c groupEvent, List<ClientServiceItemModel> clients) {
        if (clients == null || clients.isEmpty()) {
            return;
        }

        List<Group_Client__c> insClients= new List<Group_Client__c>();

        for(ClientServiceItemModel client : clients){
            Group_Client__c groupClient  = newGroupClientFrom(groupEvent.Id, client);
            insClients.add(groupClient);
        }

        if (!insClients.isEmpty())
            insert insClients;
    }

    private Group_Client__c newGroupClientFrom(String groupEventId, ClientServiceItemModel client) {
         return new Group_Client__c(
            Group_Event__c = groupEventId,
            Client__c = client.contact == null ? null : client.contact.id,
            //Service_Agreement_Item__c = (client.serviceAgreementItem == null || String.isBlank(client.serviceAgreementItem.Id)) ? null : client.serviceAgreementItem.Id,
            //Service__c = (client.service == null || String.isBlank(client.service.Id)) ? null : client.service.Id,
            //Rate__c = (client.rate == null || String.isBlank(client.rate.Id)) ? null : client.rate.Id,
            //Program__c = (client.program == null || String.isBlank(client.program.Id)) ? null : client.program.Id,
            //Site__c = (client.site == null || String.isBlank(client.site.Id)) ? null : client.site.Id,
            //Delivery_Method__c = client.deliveryMethod,
            //Local_Service_Name__c = client.serviceAgreementItem.name,
            Quantity__c = client.quantity
        );
    }

    protected override List<PickListItem> getDeliveryMethods() {
        return skedCCUtils.getPicklistValues('Group_Event__c', 'sked_Delivery_Method__c');
    }

    /**
    * @description Search all contacts with record type = 'Employee'
    * @param searchTerm for search
    * @return list<CordinatorModel> 
    */
    private List<CordinatorModel> searchCoordinator(string searchTerm){

        List<CordinatorModel> rtn = new list<CordinatorModel>();

        if (searchTerm != null) {
            String searchTermLike = skedCCJobSchedulingUtils.buildLikeSearchString(searchTerm);

            Map<Id, Contact> mpContacts = new Map<Id, Contact>([
                SELECT Id, Name, AccountID, PhotoUrl
                FROM Contact
                WHERE Name LIKE :searchTermLike AND RecordType.Name IN (:skedCCConstants.CONTACT_RECORD_TYPE_RESOURCE, :skedCCConstants.CONTACT_RECORD_TYPE_VOLUNTEER)
                ORDER BY Name
                LIMIT 100
            ]);

            if (!mpContacts.isEmpty()) {
                Map<Id, Id> mpClientIdToContentVersionId = skedCCUtils.getClientPhotoIds(mpContacts.keySet());

                CordinatorModel coordinator;

                for(Contact con : mpContacts.values()) {
                    coordinator = new CordinatorModel(con);

                    if (mpClientIdToContentVersionId.containsKey(con.Id)) {
                        coordinator.photoUrl = skedCCUtils.getPhotoUrlFromContentVersionId(mpClientIdToContentVersionId.get(con.Id));
                    }
                    rtn.add(coordinator);
                }
            }
        }
        return rtn;
    }

    private GroupEventModel buildGroupEventModel(String groupEventId) {
        Group_Event__c groupEvent = skedCCJobSchedulingUtils.getGroupEventDetails(groupEventId);
        GroupEventModel groupEventModel = null;
        if (groupEvent != null) {
            groupEventModel = new GroupEventModel(groupEvent);
            Set<String> setClientIDs = new Set<String>();

            for (ClientServiceItemModel client : groupEventModel.clients) {
                setClientIDs.add(client.contact.id);
            }

            if (!setClientIDs.isEmpty()) {
                Map<String, String> mapClientPhotos = getContactPhoto(setClientIDs);

                if (mapClientPhotos != null) {
                    for (ClientServiceItemModel client : groupEventModel.clients) {
                        if (mapClientPhotos.containsKey(client.contact.id)) {
                            client.contact.photoUrl = mapClientPhotos.get(client.contact.id);
                        }
                    }
                }
            }
        }

        return groupEventModel;
    }

    private void handleGroupEventInsert(GroupEventModel groupEvent) {
        System.debug('groupEvent = ' + groupEvent);
        GroupEventRequirementModel requirement;
        if (groupEvent.requirements != null && !groupEvent.requirements.isEmpty()) {
            requirement = groupEvent.requirements[0];
        }

        Group_Event__c skedGroupEvent = skedCCJobSchedulingUtils.groupEventModelToSkedGroupEvent(groupEvent);

        insert skedGroupEvent;
        // Insert group clients
        insertGroupClients(skedGroupEvent, groupEvent.clients);

        // Insert group tags
        if (requirement != null) {
            insertGroupTagsForGroupEvent(skedGroupEvent, requirement.tags);
        }

        groupEvent.Id = skedGroupEvent.Id;
    }

    private void createShiftOffers(List<String> lstResIds, List<String> lstJobIds) {
        Map<Id, Shift_Offer__c> mpRsSo = new Map<Id, Shift_Offer__c>();             
        Set<String> setResIds = new Set<String>(lstResIds);

        for ( String rsId : setResIds ) {
            for (String jobId : lstJobIds) {
                Shift_Offer__c so = new Shift_Offer__c();
                so.Job__c = jobId;
                so.Date_Time_Offered__c = System.now();
                so.Resource__c = rsId;
                so.Status__c = 'Offered';    
                mpRsSo.put(rsId, so);
            }
        }

        if ( mpRsSo.values() != null && mpRsSo.values().size() >0 ) {
            insert mpRsSo.values();
            List<sked__Job__c> lstJobs = [SELECT Id, Name, sked__Finish__c, sked__Start__c, sked_Shift_Offer_Status__c 
                                FROM sked__Job__c 
                                WHERE Id IN :lstJobIds];
            if (lstJobs != null) {
                for (sked__Job__c job : lstJobs) {
                    if ( job.sked_Shift_Offer_Status__c == null) {
                        job.sked_Shift_Offer_Status__c = 'Offered';
                        job.sked__Job_Status__c = 'Pending Allocation';
                    }    
                }
                sendOfferEmail(setResIds, lstJobs);    
            }
        }

    }

    //send offer email to resources
    private void sendOfferEmail(Set<String> resourceIds, list<sked__JOb__c> jobs){
        System.debug('aaaa resourceIds = ' + resourceIds);
        list<sked__Resource__c> resources = [Select Id, Name, sked__Email__c, 
                                                (Select Id, Job__r.Name, Job__r.sked__Start__c, Job__r.sked__Finish__c, Date_Time_Offered__c 
                                                    from Shift_Offers__r 
                                                    where Job__c IN :jobs
                                                    AND Status__c =: skedConstants.OFFER_OFFFERED) 
                                                from sked__Resource__c 
                                                where Id IN :resourceIds 
                                                and sked__Email__c!=null];
        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
        for(sked__Resource__c resource : resources){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { resource.sked__Email__c };
            message.subject = 'Shift Offer of ';
            String body = 'Hi ' + resource.name  + ', <br/><br/>';
            body += 'Shift Offer(s) have been assigned to you. <br/><br/>';
            body += '<table cellpadding="5" border="1" style="border-collapse: collapse"><tr><td>JOB NAME</td> <td>OFFERED TIME</td> <td>START TIME</td><td>END TIME</td>';
            for(Shift_Offer__c so: resource.Shift_Offers__r){
                message.subject += so.Job__r.Name + ', ';
                body += '<tr><td><a href="' + System.URL.getSalesforceBaseURL().toExternalForm() + '/' + so.Id + '">' + so.Job__r.Name + '</a></td><td>' + so.Date_Time_Offered__c.format() + '</td><td>' + so.Job__r.sked__Start__c.format() + '</td><td>' + so.Job__r.sked__Finish__c.format() + '</td><tr>';
            }
            message.subject = message.subject.removeEnd(', ');
            body += '</table><br/><br/>';
            body += 'To accept or decline this shift please go to shift offers in the Skedulo Mobile App. <br/><br/>';
            body += 'Best Regards,<br/>';
            body += UserInfo.getName();

            message.htmlBody = body;

            messages.add(message);
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        /*if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }*/
    }

    //check duplicate offer when get available resource for offer
    public skedResponse checkAvaiResourceForOffer(String jobId, boolean isCreateOffer) {
        JobModel jobModel = buildJobModelForJob(jobId);
        skedResponse response = getAvailableResources(jobModel);
        List<resourceAllocation> avaiResources = (List<resourceAllocation>)response.data;
        Map<String, resourceAllocation> mapAvaiResources = new Map<String, resourceAllocation>();
        
        try {
            if (avaiResources != null && !avaiResources.isEmpty()) {
                for (resourceAllocation res : avaiResources) {
                    mapAvaiResources.put(res.id, res);
                }    
                List<Shift_Offer__c> lstSentOffers = [Select Id, Resource__c, Resource__r.Name, Job__c 
                                                        from Shift_Offer__c 
                                                        where Job__c=:jobId 
                                                        and Resource__c IN :mapAvaiResources.keySet()
                                                        and Status__c != :skedConstants.OFFER_DECLINED
                                                        and Status__c != :skedConstants.OFFER_CANCELLED
                                                        ];
                if (lstSentOffers != null && !lstSentOffers.isEmpty()) {
                    for (Shift_Offer__c offer : lstSentOffers) {
                        if (mapAvaiResources.containsKey(offer.Resource__c)) {
                            if (isCreateOffer) {
                                System.debug('1 remove by offer = ' + offer);
                                mapAvaiResources.remove(offer.Resource__c);    
                            }
                            else {
                                if (offer.status__c == skedConstants.OFFER_OFFFERED) {
                                    System.debug('2 remove by offer = ' + offer);
                                    mapAvaiResources.remove(offer.Resource__c); 
                                }
                            }
                        }
                    }       
                }
                System.debug('bbb mapAvaiResources = ' + mapAvaiResources.size());
                response.data = mapAvaiResources.values();
                response.success = true;
            }
        }
        catch (Exception ex) {
            response.errorMessage += ', ' + ex.getMessage();
            response.devMessage += ', ' + ex.getStackTraceString();
        }
        
        return response;
    }

    private void handleGroupEventUpdate(GroupEventModel groupEvent) {
        GroupEventRequirementModel requirement;
        if (groupEvent.requirements != null && !groupEvent.requirements.isEmpty()) {
            requirement = groupEvent.requirements[0];
        }

        Group_Event__c skedGroupEvent = skedCCJobSchedulingUtils.groupEventModelToSkedGroupEvent(groupEvent);
        skedGroupEvent.Id = groupEvent.Id;
        update skedGroupEvent;

        // Update group clients is handled in addClient & removeClient

        // Update group tags
        if (requirement != null) {
            updateGroupTagForGroupEvent(skedGroupEvent, requirement.tags);
        } else {
            updateGroupTagForGroupEvent(skedGroupEvent, new List<Id>());
        }
    }

    protected override void handleJobInsert(JobModel jobModel) {
        // Need to populate some fields from group event
        if (String.isNotBlank(jobModel.groupEventId)) {
            GroupEventModel groupEventModel = buildGroupEventModel(jobModel.groupEventId);
            populateFieldsFromGroupEvent(jobModel, groupEventModel);
        }

        super.handleJobInsert(jobModel);

        Map<Id, Contact> clientMap = getClientMap(jobModel);
        if (!clientMap.isEmpty()) {
            // Create group Attendees
            Map<Id, Group_Attendee__c> clientToGroupAtt = insertGroupAttendeesForGroupEventJob(jobModel, clientMap);
            // Create job service item
            //insertJobServiceItems(jobModel, clientToGroupAtt);
        }

    }

    protected override void handleJobUpdate(JobModel jobModel) {
        // Update group Attendees
        Map<Id, Contact> clientMap = getClientMap(jobModel);
        Map<Id, Group_Attendee__c> clientToGroupAtt = updateGroupAttendeesForGroupEventJob(jobModel, clientMap);
        // Update job service item
        //updateJobServiceItems(jobModel, clientToGroupAtt);
        
        super.handleJobUpdate(jobModel);
    }

    protected override void populateExtraFieldsOnJob(sked__Job__c skedJob, JobModel jobModel) {
        skedJob.sked__Type__c = 'Group Event';
    }

    private void populateFieldsFromGroupEvent(JobModel jobModel, GroupEventModel groupEventModel) {
        jobModel.address = groupEventModel.address;
        jobModel.contactId = (groupEventModel.coordinator != null && String.isNotBlank(groupEventModel.coordinator.Id)) ? groupEventModel.coordinator.id : null;
        jobModel.location = groupEventModel.location;
        jobModel.region = groupEventModel.region;
        jobModel.addressType = groupEventModel.addressType;
        jobModel.account = groupEventModel.account;
        jobModel.location = groupEventModel.location;
        //jobModel.site = groupEventModel.site;
    }

   
    private void insertGroupTagsForGroupEvent(Group_Event__c groupEvent, List<Id> tagIds) {
        insertGroupTagsForGroupEvents(new List<Group_Event__c>{groupEvent}, tagIds);
    }

    private void insertGroupTagsForGroupEvents(List<Group_Event__c> groupEvents, List<Id> tagIds) {
        if (tagIds != null && !tagIds.isEmpty()) {
            List<Group_Tag__c> insertGroupTags = new List<Group_Tag__c>();
            Group_Tag__c groupTag;
            for (Group_Event__c groupEvent : groupEvents) {
                for (Id tagId : tagIds) {
                    groupTag = new Group_Tag__c(
                        Group_Event__c = groupEvent.Id,
                        Tag__c = tagId
                    );
                    insertGroupTags.add(groupTag);
                }
            }
            if (!insertGroupTags.isEmpty()) {
                insert insertGroupTags;
            }
        }
    }

    private void updateGroupTagForGroupEvent(Group_Event__c groupEvent, List<Id> tagIds) {
        updateGroupTagForGroupEvents(new List<Group_Event__c>{groupEvent}, tagIds);
    }

    private void updateGroupTagForGroupEvents(List<Group_Event__c> groupEvents, List<Id> tagIds) {
        List<Group_Event__c> existingGroups = [
            SELECT Id, Name, 
            (SELECT Id, Tag__c, Tag__r.Name FROM Group_Tags__r)
            FROM Group_Event__c
            WHERE Id IN :groupEvents
        ];

        list<Group_Tag__c> insTags= new list<Group_Tag__c>();
        list<Group_Tag__c> delTags= new list<Group_Tag__c>();

        for(Group_Event__c groupEvent : existingGroups) {
            Set<String> originalTags = new Set<string>();
            for(Group_Tag__c gTag : groupEvent.Group_Tags__r ) {
                originalTags.add(gTag.Tag__c);
            }

            Set<Id> upTagIds = new Set<Id>();

            if (tagIds != null && !tagIds.isEmpty()) {
                for( Id tagId : tagIds) {
                    if (!originalTags.contains(tagId)){
                        Group_Tag__c gTag = new Group_Tag__c(
                            Tag__c = tagId,
                            Group_Event__c = groupEvent.Id            
                        );
                        insTags.add(gTag);
                    }
                    else
                    {
                        upTagIds.add(tagId);
                    }
                }
            }

            for( Group_Tag__c gTag : groupEvent.Group_Tags__r ){
                if (!upTagIds.contains(gTag.Tag__c))
                {
                    delTags.add(gTag);
                }
            }
        }

        insert insTags;
        delete delTags;
    }

    private void cancelJobAction(String jobId, String reason, skedResponse response) {
        List<Sked__Job__c> jobs = [
            SELECT id, sked__Job_Status__c, sked__Abort_Reason__c,
                (SELECT Id, sked__Status__c
                    FROM sked__Job_Allocations__r
                    WHERE sked__Status__c != :skedCCConstants.JOB_ALLOCATION_STATUS_DELETED )
            FROM sked__Job__c WHERE id=:jobId
        ];

        if(jobs.size() == 1){
            if (jobs[0].sked__Job_Status__c == skedCCConstants.JOB_STATUS_IN_PROGRESS
                || jobs[0].sked__Job_Status__c == skedCCConstants.JOB_STATUS_COMPLETE
                || jobs[0].sked__Job_Status__c == skedCCConstants.JOB_STATUS_CANCELLED) {
                // Not allow to cancel a job with status in-proress, complete or cancel
                response.success = false;
                response.errorMessage = 'Cannot cancel a job with status: \"' + jobs[0].sked__Job_Status__c + '\"';
                return;
            }

            for(sked__Job_Allocation__c JA: jobs[0].sked__Job_Allocations__r){
                JA.sked__Status__c = skedCCConstants.JOB_ALLOCATION_STATUS_DELETED ;
            }

            update jobs[0].sked__Job_Allocations__r;

            jobs[0].sked__Job_Status__c = skedCCConstants.JOB_STATUS_CANCELLED;
            jobs[0].sked__Abort_Reason__c = reason;
            update jobs[0];                
        }
    }

    private Map<Id, Group_Attendee__c> insertGroupAttendeesForGroupEventJob(JobModel jobModel, Map<Id, Contact> clientMap) {
        List<Group_Attendee__c> newGroupAtts = new List<Group_Attendee__c>();
        Group_Attendee__c groupAtt;
        Contact client;
        Map<Id, Group_Attendee__c> clientToGroupAtt = new Map<Id, Group_Attendee__c>();

        for (ClientServiceItemModel clientService : jobModel.clients) {
            if (clientMap.containsKey(clientService.contact.Id)) {
                client = clientMap.get(clientService.contact.Id);

                groupAtt = new Group_Attendee__c(
                    Job__c = jobModel.id,
                    Contact__c = client.Id,
                    Account__c = client.AccountId
                );

                newGroupAtts.add(groupAtt);
                clientToGroupAtt.put(client.Id, groupAtt);
            }
        }

        if (!newGroupAtts.isEmpty()) {
            insert newGroupAtts;
        }

        return clientToGroupAtt;
    }

    private Map<Id, Group_Attendee__c> updateGroupAttendeesForGroupEventJob(JobModel jobModel, Map<Id, Contact> clientMap) {
        List<Group_Attendee__c> newGroupAtts = new List<Group_Attendee__c>();
        List<Group_Attendee__c> deleteGroupAtts = new List<Group_Attendee__c>();

        Group_Attendee__c newGroupAtt;
        Contact client;
        Map<Id, Group_Attendee__c> clientToGroupAtt = new Map<Id, Group_Attendee__c>();

        List<Group_Attendee__c> existingGroupAtts = [
            SELECT Id, Job__c, Contact__c, Account__c
            FROM Group_Attendee__c
            WHERE Job__c = :jobModel.Id
        ];

        Map<Id, Group_Attendee__c> existingClientToGroupAtt = new Map<Id, Group_Attendee__c>();
        Set<Id> updGroupAttIds = new Set<Id>();

        for (Group_Attendee__c groupAtt : existingGroupAtts) {
            if (groupAtt.Contact__c != null) {
                existingClientToGroupAtt.put(groupAtt.Contact__c, groupAtt);
            }
        }

        if (jobModel.clients != null) {
            for (ClientServiceItemModel clientService : jobModel.clients) {
                if (clientMap.containsKey(clientService.contact.Id)) {
                    client = clientMap.get(clientService.contact.Id);

                    if (!existingClientToGroupAtt.containsKey(client.Id)) {
                        // New group attendee
                        newGroupAtt = new Group_Attendee__c(
                            Job__c = jobModel.id,
                            Contact__c = client.Id,
                            Account__c = client.AccountId
                        );

                        newGroupAtts.add(newGroupAtt);
                        clientToGroupAtt.put(client.Id, newGroupAtt);
                    } else {
                        // Existing group attendees
                        clientToGroupAtt.put(client.Id, existingClientToGroupAtt.get(clientService.contact.Id));
                        updGroupAttIds.add(client.Id);
                    }
                }
            }
        }

        for (Group_Attendee__c groupAtt : existingGroupAtts) {
            if (groupAtt.Contact__c == null || !updGroupAttIds.contains(groupAtt.Contact__c)) {
                deleteGroupAtts.add(groupAtt);
            }
        }

        if (!newGroupAtts.isEmpty()) {
            insert newGroupAtts;
        }

        if (!deleteGroupAtts.isEmpty()) {
            delete deleteGroupAtts;
        }

        return clientToGroupAtt;
    }

}