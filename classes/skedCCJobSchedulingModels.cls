global virtual class skedCCJobSchedulingModels extends skedCCModels {
	public class ConfigData {
        public List<TagModel> tags;
        public List<RegionModel> regions;
        //public List<RoleModel> resourceRoles;
        public List<PickListItem> urgencies;
        public List<PickListItem> serviceDeliveryMethods;
        public List<PickListItem> addressTypes;
        public List<PickListItem> statuses;
        public List<PickListItem> abortReasons;

        public String jobURL;
        public String numOfResourcesMode;
        public String numOfAttendeesMode;
        public Integer intStartTime;
        public Integer intEndTime;
        public Integer intDuration;
        public Integer defaultDuration;

        public Boolean allowGetResourceCosting;
        public Boolean allowSelectRole;

        public Boolean allowSelectNumberOfResourcesRequired;
        public Boolean allowSelectNumberOfParticipants;

        // No of Attendees & Resource mode
        transient final String MIN_MODE = 'min';
        transient final String MAX_MODE = 'max';

        public ConfigData() {
            this.tags = new List<TagModel>();
            this.regions = new List<RegionModel>();
            //this.resourceRoles = new List<RoleModel>();
            this.urgencies = new List<PickListItem>();
            this.serviceDeliveryMethods = new List<PickListItem>();
            this.addressTypes = new List<PickListItem>();
            this.statuses = new List<PicklistItem>();
            this.jobURL = skedCCConfigs.JOB_URL;
            this.defaultDuration = skedCCConfigs.DEFAULT_JOB_DURATION;

            List<sked__Tag__c> tags = [SELECT Id, Name FROM sked__Tag__c Order By Name ASC];
            for (sked__Tag__c tag : tags) {
                this.tags.add(new TagModel(tag));
            }

            List<sked__Region__c> regions = [SELECT Id, Name, sked__Timezone__c FROM sked__Region__c Order By Name ASC];
            for (sked__Region__c region : regions) {
                this.regions.add(new RegionModel(region));
            }

            // Build urgency
            this.urgencies = skedCCUtils.getPicklistValues('sked__Job__c', 'sked__Urgency__c');

            // Build address type
            PicklistItem newItem;
            String[] addressType = skedCCConstants.ADDRESS_TYPE.split(',');
            Boolean itemSelected = false;

            for (String aType : addressType) {
                aType = aType.trim();
                newItem = new PicklistItem(aType, aType);
                this.addressTypes.add(newItem);
                if (!itemSelected) {
                    if (skedCCConstants.ADDRESS_TYPE_LOCATION.equalsIgnoreCase(aType) || skedCCConstants.ADDRESS_TYPE_ACCOUNT.equalsIgnoreCase(aType)) {
                        newItem.selected = true;
                        itemSelected = true;
                    }
                }
            }

            //  event statuses, default to Active
            this.statuses = skedCCUtils.getPicklistValues('Group_Event__c', 'Group_Status__c');
            String defaultStatus = 'Active';
            for (PickListItem status : statuses) {
                if (defaultStatus.equalsIgnoreCase(status.id)) {
                    status.selected = true;
                }
            }

            this.abortReasons = skedCCUtils.getPicklistValues('sked__Job__c', 'sked__Abort_Reason__c');
            this.numOfResourcesMode = MIN_MODE;
            this.numOfAttendeesMode = MIN_MODE;
            // No of Attendees & Resource mode (min/max);
            skedGroupEventCustomFields__c groupEventSettings = skedGroupEventCustomFields__c.getOrgDefaults();
            if(groupEventSettings != NULL) {
                String attMode = groupEventSettings.No_of_Attendees_Mode__c;
                if (validateMode(attMode)) {
                    this.numOfAttendeesMode = attMode;
                }

                String resourceMode = groupEventSettings.No_of_Resource_Mode__c;
                if (validateMode(resourceMode)) {
                    this.numOfResourcesMode = resourceMode;
                }
                this.allowSelectNumberOfResourcesRequired = groupEventSettings.Number_of_Resources_Required__c;
                this.allowSelectNumberOfParticipants = groupEventSettings.Maximum_Number_of_Participants__c;
            }

        }

        private Boolean validateMode(String mode) {
            return MIN_MODE.equalsIgnoreCase(mode) || MAX_MODE.equalsIgnoreCase(mode);
        }
    }

    global class ServiceRequestModel {
        public String contactId;
        public String categoryId;
        public String serviceId;
        public String siteId;
        public String programId;
        public String serviceAgreementItemId;
        public String dateIsoFormat;
    }

    global virtual class CategoryBaseModel {
        public String id;
        public String name;
        public Boolean selected;

        public CategoryBaseModel() {

        }

        public CategoryBaseModel(String id, String name) {
            this.id = id;
            this.name = name;
            this.selected = false;
        }
    }

    global virtual class JobServiceItemModel {
        public String id;
        public String deliveryMethod;
        public List<CategoryModel> categories;
        public CategoryBaseModel service;
        public CategoryBaseModel program;
        public CategoryBaseModel site;
        public CategoryBaseModel serviceAgreementItem;
        public CategoryBaseModelExt category;
        public CategoryBaseModel rate;
        public Integer quantity;
    }

    global class ClientServiceItemModel extends JobServiceItemModel {
        public ClientModel contact;
    }

    global class ClientServiceValidationInputModel {
        public List<skedCCJobSchedulingModels.ClientServiceItemModel> clientServices;
        public String dateIsoFormat;
    }

    public class CategoryModel extends CategoryBaseModel {
        //public List<CategoryBaseModel> serviceAgreementItems;
        //public ServiceModel service;
        public Boolean restrictToItems;

        public CategoryModel(String id, String name) {
            super(id, name);
            //this.serviceAgreementItems = new List<CategoryBaseModel>();
            this.restrictToItems = false;
        }
    }

    public class CategoryBaseModelExt extends CategoryBaseModel {
        public Boolean restrictToItems;

        public CategoryBaseModelExt(String id, String name) {
            this.id = id;
            this.name = name;
            this.selected = false;
            this.restrictToItems = false;
        }
    }


    public class ProgramModel extends CategoryBaseModel {
        //public List<ServiceAgreementItemModel> serviceAgreementItems;

        public ProgramModel(String id, String name) {
            super(id, name);
            //this.serviceAgreementItems  = new List<ServiceAgreementItemModel>();
        }
    }

    public class ServiceAgreementItemModel extends CategoryBaseModel {
        public List<CategoryBaseModel> rates;

        public ServiceAgreementItemModel(String id, String name) {
            super(id, name);
            this.rates = new List<CategoryBaseModel>();
        }
    }

    global virtual class BaseEventModel {
        public String id;
        public AccountModel account;
        public LocationModel location;
        //public EnriteSiteModel site;
        public String description;
        public RegionModel region;
        public String addressType;
        public JobLocation address;
        public List<ClientServiceItemModel> clients;
    }

    global class JobModel extends BaseEventModel {
        public String groupEventId;
        public String contactId;
        public String startDate;
        public Integer startTime;
        public Integer duration;
        public String urgency;
        public Boolean unscheduled;
        public Integer noOfAttendees;
        public Decimal nRequired;
        public String site;
        public List<ResourceRequirementModel> resourceRequirements;
        public List<JobAllocationModel> allocations;
        public List<JobServiceItemModel> services;

        public JobModel() {

        }

        public JobModel(sked__Job__c skedJob) {
            this.id = skedJob.id;
            this.contactId = skedJob.sked__Contact__c;
            this.description = skedJob.sked__Description__c;
            this.unscheduled = skedJob.Unscheduled_Job__c;
            this.urgency = skedJob.sked__Urgency__c;
            this.startTime = Integer.valueOf(skedJob.sked__Start__c.format(skedCCConstants.INT_TIME_FORMAT, skedJob.sked__Timezone__c));
            this.startDate = skedJob.sked__Start__c.format(skedUtils.DATE_FORMAT, skedJob.sked__Timezone__c);
            this.duration = (Integer)skedJob.sked__Duration__c;
            this.region = new RegionModel(skedJob.sked__Region__r);
            this.nRequired = skedJob.sked_No_of_Required_Resource__c;
            if (skedJob.sked__Account__c != null) {
                this.account = new AccountModel(skedJob.sked__Account__r);
            }

            if (skedJob.sked__Location__c != null) {
                this.location = new LocationModel(skedJob.sked__Location__r);
            }

            this.addressType = skedJob.sked_Address_Type__c;

            this.address = new JobLocation();
            this.address.latitude = skedJob.sked__GeoLocation__latitude__s;
            this.address.longitude = skedJob.sked__GeoLocation__longitude__s;
            this.address.fullAddress = skedJob.sked__Address__c;
            this.groupEventId = skedJob.Group_Event__c;

            this.resourceRequirements = new List<ResourceRequirementModel>();
            ResourceRequirementModel requirementModel = new ResourceRequirementModel();

            // asset
            requirementModel.asset = (Integer)skedJob.Number_of_Asset__c;
            //tags
            if (skedJob.sked__JobTags__r != null) {
                for (sked__Job_Tag__c jobTag : skedJob.sked__JobTags__r) {
                    requirementModel.tags.add(jobTag.sked__Tag__c);
                }
            }

            sked__Slot__c slot = skedCCJobSchedulingUtils.getSkedSlotFromJob(skedJob.Id);
            if (slot != null) {
                requirementModel.noOfResources = (Integer)slot.sked__Quantity__c;
                //requirementModel.Role = slot.Role__c;
            }
            this.resourceRequirements.add(requirementModel);

            // Build allocations
            this.allocations = new List<JobAllocationModel>();

            JobAllocationModel jobAllocation;
            if (skedJob.sked__Job_Allocations__r != null) {
                for (sked__Job_Allocation__c currentAlloc : skedJob.sked__Job_Allocations__r) {
                    jobAllocation = new JobAllocationModel();
                    jobAllocation.id = currentAlloc.Id;
                    jobAllocation.resource = new resource(currentAlloc.sked__Resource__r);
                    jobAllocation.status = currentAlloc.sked__Status__c;
                    jobAllocation.isTeamLead = currentAlloc.sked__Team_Leader__c;

                    this.allocations.add(jobAllocation);
                }
            }

            // job service items
            services = new List<JobServiceItemModel>();
            clients = new List<ClientServiceItemModel>();
        }    
    }

    global class GroupEventModel extends BaseEventModel {
        public String name;
        public String status;
        public ClientModel coordinator;
        // requirements
        public List<GroupEventJobListModel> jobs;
        public List<GroupEventRequirementModel> requirements;

        public GroupEventModel() {

        }

        public GroupEventModel (Group_Event__c groupEvent) {
            this.id = groupEvent.id;
            this.name = groupEvent.Name;
            this.status = groupEvent.Group_Status__c;
            this.description = groupEvent.Description__c;
            if (groupEvent.Region__c != null) {
                this.region = new RegionModel(groupEvent.Region__r);
            }

            if (groupEvent.Coordinator__c != null) {
                this.coordinator = new ClientModel();
                this.coordinator.id = groupEvent.Coordinator__c;
                this.coordinator.name = groupEvent.Coordinator__r.Name;
            }

            if (groupEvent.Account__c != null) {
                this.account = new AccountModel(groupEvent.Account__r);
            }

            if (groupEvent.Location__c != null) {
                this.location = new LocationModel(groupEvent.Location__r);
            }


            this.addressType = groupEvent.sked_Address_Type__c;

            this.address = new JobLocation();
            this.address.latitude = groupEvent.GeoLocation__latitude__s;
            this.address.longitude = groupEvent.GeoLocation__longitude__s;
            this.address.fullAddress = groupEvent.Address__c;

            // Build requirements
            this.requirements = new List<GroupEventRequirementModel>();

            GroupEventRequirementModel requirement = new GroupEventRequirementModel();
            requirement.noOfResources = (groupEvent.Number_of_Resources_Required__c == null) ? 0 : Integer.valueOf(groupEvent.Number_of_Resources_Required__c);
            requirement.noOfAttendees = (groupEvent.Number_of_Attendees__c == null) ? 0 : Integer.valueOf(groupEvent.Number_of_Attendees__c);
            requirement.asset = (groupEvent.Number_of_Asset__c == null) ? 0 : Integer.valueOf(groupEvent.Number_of_Asset__c);
            //requirement.role = groupEvent.Role__c;
            requirement.tags = new List<Id>();

            //tags
            if (groupEvent.Group_Tags__r != null) {
                for (Group_Tag__c gTag : groupEvent.Group_Tags__r) {
                    requirement.tags.add(gTag.Tag__c);
                }
            }
            requirements.add(requirement);

            // Build clients
            this.clients = new List<ClientServiceItemModel>();
            ClientServiceItemModel client;
            Set<Id> contactIds = new Set<Id>();

            for (Group_Client__c groupClient : groupEvent.Group_Clients__r) {
                contactIds.add(groupClient.Client__c);
            }

            Map<Id, Contact> contactMap = skedCCJobSchedulingUtils.getContactMapFromSet(contactIds);
            Map<Id, Id> mpClientIdToContentVersionId = skedCCUtils.getClientPhotoIds(contactIds);

            for (Group_Client__c groupClient : groupEvent.Group_Clients__r) {
                client = new ClientServiceItemModel();
                client.quantity = (groupClient.Quantity__c == null) ? 0 : Integer.valueOf(groupClient.Quantity__c);
                if (contactMap.containsKey(groupClient.Client__c)) {
                    client.contact = new ClientModel(contactMap.get(groupClient.Client__c));
                    client.contact.address = new JobLocation();
                    if(groupClient.Client__r.MailingAddress!=null){
                        client.contact.address.fullAddress = skedUtils.combineAddress( groupClient.Client__r.MailingAddress.getStreet(), groupClient.Client__r.MailingAddress.getCity(), groupClient.Client__r.MailingAddress.getState(), groupClient.Client__r.MailingAddress.getPostalCode(), groupClient.Client__r.MailingAddress.getCountry() );
                        client.contact.address.latitude = groupClient.Client__r.MailingAddress.getLatitude();
                        client.contact.address.longitude = groupClient.Client__r.MailingAddress.getLongitude();
                    }

                    if (mpClientIdToContentVersionId.containsKey(groupClient.Client__c)) {
                        client.contact.photoUrl = skedCCUtils.getPhotoUrlFromContentVersionId(mpClientIdToContentVersionId.get(groupClient.Client__c));

                    }
                }

                // service item
                client.id = groupClient.Id;
                client.deliveryMethod = groupClient.Delivery_Method__c;

                clients.add(client);
            }

            // Build jobs
            jobs = new List<GroupEventJobListModel>();

            for (sked__Job__c skedJob : groupEvent.Jobs__r) {
                jobs.add(new GroupEventJobListModel(skedJob));
            }
        }
    }

    protected Map<Id, Contact> getClientMap(JobModel jobModel) {
        Map<Id, Contact> clientMap = new Map<Id, Contact>();

        if (jobModel.clients != null && !jobModel.clients.isEmpty()) {
            Set<Id> clientIds = new Set<Id>();
            for (ClientServiceItemModel clientService : jobModel.clients) {
                if (clientService.contact != null && String.isNotBlank(clientService.contact.id)) {
                    clientIds.add(clientService.contact.id);
                }
            }
            if (!clientIds.isEmpty()) {
                clientMap = skedCCJobSchedulingUtils.getContactMapFromSet(clientIds);
            }
        }
        return clientMap;
    }

    public class GroupEventJobListModel {
        public String id;
        public String name;
        public String startDateTime;
        public String status;
        public String address;
        public String reason;
        public Integer allocationCount;
        public Integer duration;
        public Integer noOfAttendees;

        public GroupEventJobListModel(sked__Job__c skedJob) {
            this.id = skedJob.id;
            this.name = skedJob.name;
            this.startDateTime = skedJob.sked__Start__c.format('dd-MM-yyyy hh:mm a', skedJob.sked__Timezone__c);
            this.status = skedJob.sked__Job_Status__c;
            this.address = skedJob.sked__Address__c;
            this.allocationCount = Integer.valueOf(skedJob.sked__Job_Allocation_Count__c);
            this.duration = Integer.valueOf(skedJob.sked__Duration__c);
            this.noOfAttendees = Integer.valueOf(skedJob.No_of_Attendees__c);
            this.reason = skedJob.sked__Abort_Reason__c;
        }
    }

    global class JobAllocationModel {
        public String id;
        public resource resource;
        public String job;
        // Temporary store the shift cost
        // The allocation cost to show on UI
        public String status;
        public Boolean isTeamLead;
    }

    global class ResourceCostingRequestModel {
        public String startDate;
        public Integer startTime;
        public Integer duration;
        public String role;
        public List<String> resourceIds;
        public String timeZone;
        public Boolean unscheduled;
    }

    public virtual class AddressTypeModel {
        public string name;
        public string id;
        public JobLocation address;
    }

    public class AccountModel extends AddressTypeModel {

        public AccountModel(){}

        public AccountModel (Account act) {
            this.name = act.Name;
            this.id = act.Id;
            this.address = new JobLocation();
            this.address.city = act.BillingCity;
            this.address.state = act.BillingState;
            this.address.fullAddress = act.Physical_Address__c;
            if (act.Physical_GeoLocation__c != null) {
                this.address.latitude = act.Physical_GeoLocation__latitude__s;
                this.address.longitude = act.Physical_GeoLocation__longitude__s;
            }
        }

        public AccountModel(Id accountId, String name) {
            this.id = accountId;
            this.name = name;
            this.address = new JobLocation();
            this.address.fullAddress = '';
        }
    }

    public class LocationModel extends AddressTypeModel {
        public LocationModel(){}

        public LocationModel (sked__Location__c skedLocation) {
            this.name = skedLocation.Name;
            this.id = skedLocation.Id;
            this.address = new JobLocation();
            this.address.fullAddress = skedLocation.sked__Address__c;
            if (skedLocation.sked__GeoLocation__c != null) {
                this.address.latitude = skedLocation.sked__GeoLocation__latitude__s;
                this.address.longitude = skedLocation.sked__GeoLocation__longitude__s;
            }
        }
    }

    public class ClientModel {
        public String id;
        public String name;
        public String label;
        public String contactNumber;
        public String birthdate;
        public String account;
        public String photoUrl;
        public JobLocation address;
        public List<String> blacklist {get;set;}
        public List<TagModel> tags {get;set;}

        public ClientModel() {
            this.blacklist = new List<String>();
            this.tags = new List<TagModel>();
        }

        public ClientModel(Contact con) {
            this.id = con.id;
            this.name = con.name;

            if(con.Birthdate != null) {
               Datetime dtBirthDate = Datetime.newInstance(con.Birthdate.year(), con.Birthdate.month(), con.Birthdate.day());
               this.birthDate = dtBirthDate.format('dd/MM/yyyy');
               if(con.MobilePhone != null) {
                   this.label = con.name + ' - MOB ' + con.MobilePhone + ' - DOB ' + dtBirthDate.format('dd/MM/yyyy');
               }
               else {
                   this.label = con.name + ' - DOB ' + dtBirthDate.format('dd/MM/yyyy');
               }
            }
            else {
               if(con.MobilePhone != null) {
                   this.label = con.name + ' - MOB ' + con.MobilePhone ;
               }
               else {
                   this.label = con.name;
               }
            }

            this.photoUrl = con.PhotoUrl;
            this.contactNumber = con.MobilePhone;
            this.account = con.Account.Name;

            this.blacklist = new List<string>();


            this.tags = new List<TagModel>();
            for ( Client_Tag__c cTag : con.Client_Tags__r ) {
                this.tags.add(new TagModel(cTag));
            }
        }
    }

    public class JobSavingModel {
        public String jobId;

        public JobSavingModel() {
        }
    }

    public virtual class BaseRequirementModel {
        public List<Id> tags;
        public String role;
        public Integer noOfResources;
        public Integer asset;
    }

    public class ResourceRequirementModel extends BaseRequirementModel{
        public ResourceRequirementModel() {
            this.tags = new List<Id>();
        }
    }

    public class GroupEventRequirementModel extends BaseRequirementModel{
        public Integer noOfAttendees;

        public GroupEventRequirementModel() {
            this.tags = new List<Id>();
        }
    }

    public class SuggestedAddressModel {
        public String fullAddress;
        public String placeId;

        public SuggestedAddressModel(String fullAddress, String placeId) {
            this.fullAddress = fullAddress;
            this.placeId = placeId;
        }
    }
}