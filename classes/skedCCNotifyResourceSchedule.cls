global class skedCCNotifyResourceSchedule implements Schedulable{
	List<sked__Job_Allocation__c> newJoblstDispatchJAs;

	global skedCCNotifyResourceSchedule(List<sked__Job_Allocation__c> lstJas) {
		newJoblstDispatchJAs = new List<sked__Job_Allocation__c>(lstJas);
	}

	global void execute(SchedulableContext sc) {
		System.debug('aaa newJoblstDispatchJAs = ' + newJoblstDispatchJAs);
		skedCCNotifyResourcesBatch abc = new skedCCNotifyResourcesBatch(newJoblstDispatchJAs);
        Database.executeBatch(abc);
	}
}