public abstract class skedCCBaseEventCtrl extends skedCCJobSchedulingModels{
	protected abstract List<PickListItem> getDeliveryMethods();

    public skedResponse getConfigData() {
        skedResponse response = new skedResponse();
        try {
            ConfigData configData = new ConfigData();
            configData.serviceDeliveryMethods = getDeliveryMethods();

            String defaultDeliveryMethod = skedCCConfigs.DEFAULT_DELIVERY_METHOD;
            if (String.isNotBlank(defaultDeliveryMethod)) {
                for (PicklistItem deliverMethod : configData.serviceDeliveryMethods) {
                    if (defaultDeliveryMethod.equalsIgnoreCase(deliverMethod.label)) {
                        deliverMethod.selected = true;
                        break;
                    }
                }
            }

            response.success = true;
            response.data = configData;     
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse getContactInfo(String contactId) {
        skedResponse response = new skedResponse();
        try {

            List<Contact> contacts = [
                SELECT Id, MailingAddress, Mailing_Address_Latitude__c, Mailing_Address_Longitude__c, PhotoURL, 
                (select Id, Client__c, Tag__c, Tag__r.Name, Weighting__c, Required__c FROM Client_Tags__r)
                FROM Contact WHERE Id = :contactId
            ];

            JobLocation fulladdress = new JobLocation();

            ClientModel client;

            if (!contacts.isEmpty()) {
                client =  new ClientModel();
                Address contactAdd = contacts[0].MailingAddress;

                if (contactAdd != null) {
                    fullAddress.fullAddress = skedUtils.combineAddress( contactAdd.getStreet(), contactAdd.getCity(), contactAdd.getState(), contactAdd.getPostalCode(), contactAdd.getCountry() );

                    fulladdress.latitude = contactAdd.getLatitude();
                    fulladdress.longitude = contactAdd.getLongitude();
                }

                client.address = fulladdress;
                client.photoUrl = contacts[0].PhotoURL;

                client.tags = new List<TagModel>();
                for ( Client_Tag__c cTag : contacts[0].Client_Tags__r ) {
                    client.tags.add(new TagModel(cTag));
                }
            }


            response.success = true;
            response.data = client;
        }
        catch ( Exception ex ) {
            response.getErrorMessage(ex);                 
        }
        return response;
    }

    public Map<String, String> getContactPhoto (Set<String> setContactIds) {
        Map<String, String> mapContactPictures = new Map<String, String>();

        for (Attachment cdl : [SELECT Id, Name, ParentId 
                                            FROM Attachment 
                                            WHERE ParentId IN :setContactIds
                                            AND Name LIKE '%Client%Profile%Photo%']) {
            System.debug('cdl = ' + cdl);
            String photoUrl = skedCCConstants.DEFAULT_ATTACHMENT_URL + cdl.Id;
            mapContactPictures.put(cdl.ParentId, photoUrl);
        }

        return mapContactPictures;
    }   

    public skedResponse getJobDetails(String jobId) {
        skedResponse response = new skedResponse();
        try {
            JobModel jobModel = buildJobModelForJob(jobId);
            if (jobModel.clients != null && !jobModel.clients.isEmpty()) {
                Set<String> setContactIds = new Set<String>();
                for (ClientServiceItemModel client : jobModel.clients) {
                    setContactIds.add(client.contact.id);
                }

                if (!setContactIds.isEmpty()) {
                    Map<String, String> mapContactPictures = getContactPhoto(setContactIds);

                    if (mapContactPictures != null) {
                        for (ClientServiceItemModel client : jobModel.clients) {
                            if (mapContactPictures.containsKey(client.contact.id)) {
                                client.contact.photoUrl = mapContactPictures.get(client.contact.id);
                            }
                        }
                    }
                }
            }
            response.success = true;
            response.data = jobModel;
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse getAvailableResources(JobModel jobModel) {
        skedResponse response = new skedResponse();
        try {

            skedCCAvailatorParams params = skedCCJobSchedulingUtils.getAvailatorParams(jobModel);
            System.debug('aaa params = ' + params);
            skedCCAvailator validator = new skedCCAvailator(params);
            System.debug('aaa validator = ' + validator);
            List<resourceAllocation> avaiResources = validator.getAvailableResources();
            System.debug('126 avaiResources = ' + avaiResources.size());
            response.success = true;
            response.data = avaiResources;
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse getAddresses(String searchingText) {
        skedResponse response = new skedResponse();
        try {
            skedCCLocationServices.AutoCompleteAddress addresses = skedCCLocationServices.getAddresses(searchingText);
            if ( addresses != NULL ) {
                List<SuggestedAddressModel> suggestedAdds = new List<SuggestedAddressModel>();
                for (skedCCLocationServices.Prediction pred : addresses.predictions) {
                    suggestedAdds.add(new SuggestedAddressModel(pred.description, pred.place_id));
                }

                response.success = true;
                response.data = suggestedAdds;
            }
        }
        catch ( Exception ex ) {
            response.getErrorMessage(ex);                 
        }
        return response;
    }

    public skedResponse getGeolocation(String address, String placeId) {
        skedResponse response = new skedResponse();
        try {
            Location bookingLocation = skedCCLocationServices.getAddressGeoLocation(address);
            if ( Test.isRunningTest() ) bookingLocation = Location.newInstance(34.0169571, 118.2888306);

            if (bookingLocation != null) {
                JobLocation location = new JobLocation(bookingLocation);
                response.success = true;
                response.data = location;
            } else {
                response.errorMessage = skedCCConstants.INVALID_ADDRESS_ERROR_MSG;
            }  
        } catch ( Exception ex ) {
            
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse searchAccount(String term) {
        skedResponse response = new skedResponse();
        try {
            List<AccountModel> results = new List<AccountModel>();

            string searchTerm = skedCCJobSchedulingUtils.buildLikeSearchString(term);
            List<Account> accounts = skedCCJobSchedulingUtils.searchAccountByName(searchTerm);

            for (Account acc : accounts) {
                results.add(new AccountModel(acc));
            }

            response.success = true;
            response.data = results;
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse searchLocation(String term, String regionId) {
        skedResponse response = new skedResponse();
        try {
            List<LocationModel> results = new List<LocationModel>();

            string searchTerm = skedCCJobSchedulingUtils.buildLikeSearchString(term);
            List<sked__Location__c> locations = skedCCJobSchedulingUtils.searchLocationByName(searchTerm, regionId);

            for (sked__Location__c skedLoc : locations) {
                results.add(new LocationModel(skedLoc));
            }

            response.success = true;
            response.data = results;
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }
    
    public skedResponse setTeamLeader(String allocationId, Boolean teamLeader) {
        skedResponse response = new skedResponse();
        try {
            List<sked__Job_Allocation__c> allocations = [
                SELECT Id, sked__Team_Leader__c, sked__Status__c
                FROM sked__Job_Allocation__c
                WHERE Id = :allocationId
                AND sked__Status__c NOT IN (:skedCCConstants.JOB_ALLOCATION_STATUS_DELETED, :skedCCConstants.JOB_ALLOCATION_STATUS_DECLINED)
                AND sked__Team_Leader__c != :teamLeader
            ];
            if (!allocations.isEmpty()) {
                allocations[0].sked__Team_Leader__c = teamLeader;
                update allocations[0];
                response.success = true;
            } else {
                response.success = false;
            }
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse removeAllocation(JobAllocationModel allocation, JobModel jobModel) {
        skedResponse response = new skedResponse();
        try {
            List<sked__Job_Allocation__c> allocations = [
                SELECT Id, sked__Team_Leader__c, sked__Status__c, sked__Resource__c
                FROM sked__Job_Allocation__c
                WHERE Id = :allocation.Id
                AND sked__Status__c NOT IN (:skedCCConstants.JOB_ALLOCATION_STATUS_DELETED, :skedCCConstants.JOB_ALLOCATION_STATUS_DECLINED)
            ];

            if (!allocations.isEmpty()) {
                allocations[0].sked__Team_Leader__c = false;
                allocations[0].sked__Status__c = skedCCConstants.JOB_ALLOCATION_STATUS_DELETED;
                update allocations[0];

                List<resourceAllocation> avaiResources = new List<resourceAllocation>();
                if (jobModel != null) {
                    // Check if removed resource is available
                    skedCCAvailatorParams params = skedCCJobSchedulingUtils.getAvailatorParams(jobModel);
                    params.resourceIds.add(allocations[0].sked__Resource__c);
                    skedCCAvailator validator = new skedCCAvailator(params);
                    avaiResources = validator.getAvailableResources();
                }

                response.data = avaiResources;
                response.success = true;
            } else {
                response.success = false;
            }
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }

    public skedResponse notifyResource(String allocationId) {
        skedResponse response = new skedResponse();
        try {

            List<sked__Job_Allocation__c> allocations = [
                SELECT Id, sked__Status__c
                FROM sked__Job_Allocation__c
                WHERE Id = :allocationId
                AND sked__Status__c NOT IN (:skedCCConstants.JOB_ALLOCATION_STATUS_DELETED, :skedCCConstants.JOB_ALLOCATION_STATUS_DECLINED)
            ];

            // notify resource
            if (!allocations.isEmpty()) {
                skedCCNotifyResourcesBatch batch = new skedCCNotifyResourcesBatch(allocations);
                Database.executeBatch(batch);
            }

            response.success = true;
        } catch ( Exception ex ) {
            response.getErrorMessage(ex);
        }
        return response;
    }
    
    protected JobModel buildJobModelForJob(String jobId) {
        sked__Job__c skedJob = skedCCJobSchedulingUtils.getJobDetails(jobId);
        System.debug('skedJob = ' + skedJob);
        JobModel jobModel = null;
        if (skedJob != null) {
            jobModel = new JobModel(skedJob);
        }
        return jobModel;
    }

    protected virtual void handleJobInsert(JobModel jobModel) {
        sked__Job__c skedJob = skedCCJobSchedulingUtils.jobModelToSkedJob(jobModel);
        populateExtraFieldsOnJob(skedJob, jobModel);
        skedJob.sked__Job_Status__c = skedCCConstants.JOB_STATUS_PENDING_ALLOCATION;

        insert skedJob;
        jobModel.id = skedJob.id;
        // insert job tag
        insertJobTagsForJobs(new List<sked__Job__c>{skedJob}, jobModel.resourceRequirements[0].tags);

        sked__Slot__c slot = skedCCJobSchedulingUtils.toSkedSlot(jobModel);
        insert slot;
    }

    protected virtual void handleJobUpdate(JobModel jobModel) {
        sked__Job__c skedJob = skedCCJobSchedulingUtils.jobModelToSkedJob(jobModel);
        System.debug('skedJob = ' + skedJob);
        Boolean hasAllocation = (jobModel.allocations != null && !jobModel.allocations.isEmpty());
        if (hasAllocation) {
            skedJob.sked__Job_Status__c = skedCCConstants.JOB_STATUS_DISPATCHED;
        }
        update skedJob;

        // update job tags
        updateJobTagForJobs(new List<sked__Job__c>{skedJob}, jobModel.resourceRequirements[0].tags);

        // update job slot
        sked__Slot__c savedSlot = skedCCJobSchedulingUtils.getSkedSlotFromJob(skedJob.Id);
        sked__Slot__c slot = skedCCJobSchedulingUtils.toSkedSlot(jobModel);
        if (savedSlot != null) {
            slot.id = savedSlot.id;
        }

        upsert slot;

        // Job allocation
        processJobAllocations(jobModel);
    }

    protected virtual void populateExtraFieldsOnJob(sked__Job__c skedJob, JobModel jobModel) {

    }

    protected void populateJSIData(Job_Service_Item__c jsi, JobServiceItemModel newData) {
        
    }

    protected void processJobAllocations(JobModel jobModel) {
        List<sked__Job__c> skedJobs = [
            SELECT Id, Name,
            (   SELECT Id, sked__Resource__c
                FROM sked__Job_Allocations__r
                WHERE sked__Status__c NOT IN (:skedCCConstants.JOB_ALLOCATION_STATUS_DELETED, :skedCCConstants.JOB_ALLOCATION_STATUS_DECLINED))
            FROM sked__Job__c
            WHERE Id = :jobModel.id
        ];

        if (skedJobs.isEmpty()) {
            return;
        }

        sked__Job__c skedJob = skedJobs[0];

        List<sked__Job_Allocation__c> upsertAllocations = new List<sked__Job_Allocation__c>();
        // To check if allocation for a resource is kept
        Set<Id> keepResourceIds = new Set<Id>();
        Set<Id> allocatedResourceIds = new Set<Id>();
        sked__Job_Allocation__c newJobAlloc;
        String resourceId;

        for (sked__Job_Allocation__c currentAlloc : skedJob.sked__Job_Allocations__r) {
            allocatedResourceIds.add(currentAlloc.sked__Resource__c);
        }

        if (jobModel.allocations != null) {
            for (JobAllocationModel alloc : jobModel.allocations) {
                resourceId = alloc.resource.id;
                if (allocatedResourceIds.contains(resourceId)) {
                    keepResourceIds.add(resourceId);
                } else {
                    newJobAlloc = new sked__Job_Allocation__c(
                        sked__Job__c = skedJob.id,
                        sked__Resource__c = resourceId,
                        sked__Status__c = skedCCConstants.JOB_ALLOCATION_STATUS_DISPATCHED
                    );
                    upsertAllocations.add(newJobAlloc);
                }
            }
        }

        for(sked__Job_Allocation__c currentAlloc : skedJob.sked__Job_Allocations__r){
            if (!keepResourceIds.contains(currentAlloc.sked__Resource__c))
            {
                // change status to delete
                currentAlloc.sked__Status__c = skedCCConstants.JOB_ALLOCATION_STATUS_DELETED;
                upsertAllocations.add(currentAlloc);
            }
        }

        if (!upsertAllocations.isEmpty()) {
            upsert upsertAllocations; 

            /*Set<Id> setDispatchJAs = new Set<Id>();
            for (sked__Job_Allocation__c ja : upsertAllocations)  {
                if (ja.sked__Status__c == skedCCConstants.JOB_ALLOCATION_STATUS_DISPATCHED) {
                    setDispatchJAs.add(ja.Id);
                }
            }

            if (!setDispatchJAs.isEmpty()) {
                skedCCSkeduloApiManager.sendNotification(setDispatchJAs);
            }*/
        }
    }

    protected void insertJobTagsForJobs(List<sked__Job__c> jobs, List<Id> tagIds) {
        if (tagIds != null && !tagIds.isEmpty()) {
            List<sked__Job_Tag__c> insertJobTags = new List<sked__Job_Tag__c>();
            sked__Job_Tag__c jobTag;
            for (sked__Job__c skedJob : jobs) {
                for (Id tagId : tagIds) {
                    jobTag = new sked__Job_Tag__c(
                        sked__Job__c = skedJob.Id,
                        sked__Tag__c = tagId
                    );
                    insertJobTags.add(jobTag);
                }
            }
            if (!insertJobTags.isEmpty()) {
                insert insertJobTags;
            }
        }
    }

    protected void updateJobTagForJobs(List<sked__Job__c> jobs, List<Id> tagIds) {
        List<sked__Job__c> skedJobs = [
            SELECT Id, Name, 
            (SELECT Id, sked__Tag__c, sked__Tag__r.Name FROM sked__JobTags__r)
            FROM sked__Job__c
            WHERE Id IN :jobs
        ];

        list<sked__Job_Tag__c> insTags= new list<sked__Job_Tag__c>();
        list<sked__Job_Tag__c> delTags= new list<sked__Job_Tag__c>();

        for(sked__Job__c skedJob : skedJobs) {
            Set<String> originalTags = new Set<string>();
            for(sked__Job_Tag__c jTag : skedJob.sked__JobTags__r ) {
                originalTags.add(jTag.sked__Tag__c);
            }

            Set<Id> upTagIds = new Set<Id>();

            if (tagIds != null && !tagIds.isEmpty()) {
                for( Id tagId : tagIds) {
                    if (!originalTags.contains(tagId)){
                        sked__Job_Tag__c jTag = new sked__Job_Tag__c(
                            sked__Tag__c = tagId,
                            sked__Job__c = skedJob.Id                   
                        );
                        insTags.add(jTag);
                    }
                    else
                    {
                        upTagIds.add(tagId);
                    }
                }
            }

            for( sked__Job_Tag__c jTag : skedJob.sked__JobTags__r ){
                if (!upTagIds.contains(jTag.sked__Tag__c))
                {
                    delTags.add(jTag);
                }
            }
        }

        insert insTags;
        delete delTags;
    }

/*    protected List<Job_Service_Item__c> getJobServiceItems(String jobId) {
        return [
                SELECT Id, Job__c, Group_Attendee__c
                FROM Job_Service_Item__c
                WHERE Job__c = :jobId
        ];
    }*/
}