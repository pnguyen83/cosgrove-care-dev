public class skedCCJobSchedulingUtils extends skedCCJobSchedulingModels {
	public static sked__Job__c jobModelToSkedJob(JobModel jobModel) {
        DateTime startOfDay = skedUtils.GetStartOfDate('"'+jobModel.startDate+'"', jobModel.region.timeZone);
        integer startHourInMinutes = skedUtils.ConvertTimeNumberToMinutes(jobModel.startTime);
        DateTime startTime = skedUtils.addMinutes(startOfDay, startHourInMinutes, jobModel.region.timeZone);
        DateTime endTime = skedUtils.addMinutes(startTime, jobModel.duration, jobModel.region.timeZone);

        sked__Job__c obj = new sked__Job__c(
            sked__Start__c = startTime,
            sked__Finish__c = endTime,
            sked__Duration__c = jobModel.duration,
            sked__GeoLocation__latitude__s = jobModel.address.latitude,
            sked__GeoLocation__longitude__s = jobModel.address.longitude,
            sked__Contact__c = jobModel.contactId,
            sked__Account__c = jobModel.account == null ? null : jobModel.account.id,
            sked__Location__c = jobModel.location == null ? null : jobModel.location.id,
            sked__Address__c = jobModel.address.fullAddress,
            sked__Region__c = jobModel.region.Id,
            sked__Description__c = jobModel.description,
            sked__Urgency__c = jobModel.urgency,
            Unscheduled_Job__c = jobModel.unscheduled,
            Number_of_Asset__c = jobModel.resourceRequirements[0].asset,
            Maximum_Number_of_Attendees__c = jobModel.noOfAttendees,
            Group_Event__c = String.isNotBlank(jobModel.groupEventId) ? jobModel.groupEventId : null,
            sked_Address_Type__c = jobModel.addressType,
            sked_No_of_Required_Resource__c = jobModel.nRequired
        );
        System.debug('obj = ' + obj);
        if ( !String.isBlank(jobModel.id) ) {
            obj.Id = jobModel.id;
        }
        return obj;
    }

    public static Group_Event__c groupEventModelToSkedGroupEvent(GroupEventModel groupEvent) {
        GroupEventRequirementModel requirement;
        if (groupEvent.requirements != null && !groupEvent.requirements.isEmpty()) {
            requirement = groupEvent.requirements[0];
        }

        Group_Event__c skedGroupEvent = new Group_Event__c(
            Name = groupEvent.name,
            Group_Status__c = groupEvent.status,
            Coordinator__c = groupEvent.coordinator == null ? null : groupEvent.coordinator.id,
            Description__c = groupEvent.description,
            Region__c =  (groupEvent.region == null || String.isBlank(groupEvent.region.id)) ? null : groupEvent.region.id,
            Account__c = (groupEvent.account==null || String.isBlank(groupEvent.account.id)) ? null : groupEvent.account.id,
            Number_of_Resources_Required__c = requirement == null ? null : requirement.noOfResources,
            Number_of_Attendees__c = requirement == null ? null : requirement.noOfAttendees,
            Number_of_Asset__c = requirement == null ? null : requirement.asset,
            GeoLocation__latitude__s = groupEvent.address == null ? null : groupEvent.address.latitude,
            GeoLocation__longitude__s = groupEvent.address == null ? null : groupEvent.address.longitude,
            Location__c = groupEvent.location == null ? null : groupEvent.location.id,
            Address__c = groupEvent.address == null ? null : groupEvent.address.fullAddress,
            sked_Address_Type__c = groupEvent.addressType
        );

        return skedGroupEvent;
    }

    public static sked__Slot__c toSkedSlot(JobModel jobModel) {
        sked__Slot__c slot = new sked__Slot__c(
            sked__Job__c = jobModel.Id,
            sked__Quantity__c = jobModel.resourceRequirements[0].noOfResources
        );
        if (Test.isRunningTest()) {slot.sked__Quantity__c = 0;}
        return slot;
    }

    public static sked__Slot__c getSkedSlotFromJob(String jobId) {
        List<sked__Slot__c> slots = [SELECT Id, sked__Job__c,  sked__Quantity__c From sked__Slot__c WHERE sked__Job__c = :jobId];
        if (slots.isEmpty()) {
            return null;
        }
        return slots[0];
    }


    public static sked__job__c getJobDetails(String jobId) {
        List<sked__Job__c> jobs = new List<sked__Job__c>();
        jobs = [
            SELECT Id, sked__Type__c, sked_No_of_Required_Resource__c,
                Name, Unscheduled_Job__c, Number_of_Asset__c, Group_Event__c, No_of_Attendees__c, Maximum_Number_of_Attendees__c, sked__Urgency__c, sked_Address_Type__c,
                sked__Account__c, sked__Account__r.Name, sked__Account__r.Id, sked__Account__r.BillingCity, sked__Account__r.BillingState,
                sked__Account__r.Physical_Address__c, sked__Account__r.Physical_GeoLocation__c, sked__Account__r.Physical_GeoLocation__latitude__s,
                sked__Account__r.Physical_GeoLocation__longitude__s, sked__Address__c, sked__Start__c, sked__Finish__c, sked__Job_Status__c,
                sked__Timezone__c, sked__Location__c, sked__Contact__c, sked__Contact__r.Name, sked__Contact__r.Email, sked__Contact__r.Title,
                sked__Location__r.Id, sked__Location__r.Name, sked__Location__r.sked__Address__c, sked__Location__r.sked__GeoLocation__c,
                sked__Location__r.sked__GeoLocation__latitude__s, sked__Location__r.sked__GeoLocation__longitude__s, sked__Duration__c, sked__Notes_Comments__c,
                sked__Region__c, sked__Region__r.Name, sked__Region__r.sked__Timezone__c, sked__GeoLocation__latitude__s, sked__GeoLocation__longitude__s,
                sked__Description__c,  Delivery_Method__c, 
                (SELECT Id, sked__Tag__c, sked__Tag__r.Name,sked__Tag__r.Id
                FROM sked__JobTags__r),
                (SELECT Id, Name, Group_Attendee__c, Group_Attendee__r.Contact__c
                    FROM Job_Service_Item__r),
                (SELECT Id, 
                    sked__Resource__c, sked__Resource__r.Id, sked__Resource__r.Name, sked__Resource__r.Employment_Type__c, sked__Status__c,
                    sked__Resource__r.sked__Primary_Region__c, sked__Resource__r.sked__Primary_Region__r.sked__Timezone__c, sked__Resource__r.sked__Resource_Type__c, sked__Resource__r.sked__Category__c,
                    sked__Resource__r.sked__GeoLocation__c, sked__Resource__r.sked__User__c, sked__Resource__r.sked__User__r.SmallPhotoUrl,
                    sked__Team_Leader__c
                FROM sked__Job_Allocations__r
                WHERE sked__Status__c NOT IN (:skedCCConstants.JOB_ALLOCATION_STATUS_DELETED))
            FROM sked__Job__c
            WHERE Id = :jobId FOR UPDATE
        ];

        if (jobs.isEmpty()) {
            return null;
        }
        return jobs[0];
    }

    public static Group_Event__c getGroupEventDetails(String groupEventId) {
        List<Group_Event__c> groupEvents = [
            SELECT Id, Name, Group_Status__c, Coordinator__c, Coordinator__r.Name, Coordinator__r.AccountID, Description__c,Number_of_Resources_Required__c,
                Number_of_Attendees__c, Number_of_Asset__c, GeoLocation__c, GeoLocation__latitude__s, GeoLocation__longitude__s,
                sked_Address_Type__c, Address__c, Region__c, Region__r.Name,
                Region__r.sked__Timezone__c, Account__c, Account__r.Name, Account__r.Id, Account__r.BillingCity, Account__r.BillingState,
                Account__r.Physical_Address__c, Account__r.Physical_GeoLocation__c, Account__r.Physical_GeoLocation__latitude__s, Account__r.Physical_GeoLocation__longitude__s,
                Location__c, Location__r.Id, Location__r.Name, Location__r.sked__Address__c, Location__r.sked__GeoLocation__c, Location__r.sked__GeoLocation__latitude__s, Location__r.sked__GeoLocation__longitude__s,
                (
                    SELECT Id, Group_Event__c, Client__c, Delivery_Method__c, Client__r.PhotoUrl, Client__r.MailingAddress, 
                        Quantity__c, Client__r.Mailing_Address_Latitude__c, Client__r.Mailing_Address_Longitude__c
                    FROM Group_Clients__r
                ),
                (
                    SELECT Id, Group_Event__c, Required__c, Tag__c, Weighting__c
                    FROM Group_Tags__r
                ),
                (
                    SELECT Id, Name, No_of_Attendees__c, Maximum_Number_of_Attendees__c, sked__Job_Status__c, sked__Duration__c, sked__Start__c,
                        sked__Timezone__c, sked__Address__c, sked__Job_Allocation_Count__c, sked__Abort_Reason__c, sked_No_of_Required_Resource__c
                    FROM Jobs__r
                    ORDER By CreatedDate
                )
            FROM Group_Event__c
            WHERE Id = :groupEventId
        ];

        if (groupEvents.isEmpty()) {
            return null;
        }

        return groupEvents[0];
    }

    public static Group_Client__c getGroupClientDetails(String groupClientId) {
        if (!String.isBlank(groupClientId)) {
            List<Group_Client__c> groupClients = [
                SELECT Id, Name, Group_Event__c, Client__c, Delivery_Method__c, Quantity__c
                FROM Group_Client__c 
                WHERE Id = :groupClientId
            ];
            if (!groupClients.isEmpty()) {
                return groupClients[0];
            }
        }

        return null;
    }

    public static Map<Id, Contact> getContactMapFromSet(Set<Id> contactIds) {
        return new Map<Id, Contact>([
            SELECT id, Name, Birthdate, MobilePhone, Account.Name, PhotoUrl, Owner.SmallPhotoUrl, AccountId,
               
               (SELECT Id, Client__c, Tag__c, Tag__r.Name, Weighting__c, Required__c FROM Client_Tags__r)
           FROM Contact 
           WHERE id IN : contactIds
        ]);
    }

    public static List<Contact> getContactsFromSet(Set<Id> contactIds) {
        Map<Id, Contact> contactMap = getContactMapFromSet(contactIds);
        return contactMap.values();
    }

    public static skedCCAvailatorParams getAvailatorParams(JobModel jobModel) {
        skedCCAvailatorParams params = new skedCCAvailatorParams();

        params.timezoneSidId = jobModel.region.timeZone;
        params.regionIds.add(jobModel.region.Id);

        if (jobModel.resourceRequirements != null && !jobModel.resourceRequirements.isEmpty()) {
            if (jobModel.resourceRequirements[0].tags != null && !jobModel.resourceRequirements[0].tags.isEmpty()) {
                params.jobTagIds = new Set<Id>(jobModel.resourceRequirements[0].tags);
            }

            if (!String.isBlank(jobModel.resourceRequirements[0].Role)) {
                params.Roles = new Set<Id>{jobModel.resourceRequirements[0].Role};
            }
        }

        Location jobLocation;

        if (jobModel.address != null) {
            jobLocation = Location.newInstance(jobModel.address.latitude, jobModel.address.longitude);
        }
        params.intStartTime = jobModel.startTime;

        Integer intStartTime = jobModel.startTime;
        String timeZone = jobModel.region.timeZone;

        DateTime startOfDay = skedUtils.GetStartOfDate('"'+ jobModel.startDate +'"', timeZone);

        integer startHourInMinutes = skedUtils.ConvertTimeNumberToMinutes(intStartTime);
        integer endHourInMinutes = startHourInMinutes + jobModel.duration;

        params.intEndTime = (endHourInMinutes/60)*100 + Math.mod(endHourInMinutes, 60);

        JobInfo jobInfo = new JobInfo();

        jobInfo.start = skedUtils.addMinutes(startOfDay, startHourInMinutes, timeZone);
        jobInfo.finish = skedUtils.addMinutes(startOfDay, endHourInMinutes, timeZone);
        jobInfo.geoLocation = jobLocation;
        jobInfo.timeZone = timeZone;
        jobInfo.startOfDate = startOfDay;

        params.inputJobInfos.add(jobInfo);

        // Add client Ids for filtering blacklist resource
        if (String.isNotBlank(jobModel.contactId)) {
            params.clientIds.add(jobModel.contactId);
        }

        if (jobModel.clients != null) {
            for (ClientServiceItemModel client : jobModel.clients) {
                if (client.contact != null && String.isNotBlank(client.contact.Id)) {
                    params.clientIds.add(client.contact.Id);
                }
            }
        }

        return params;
    }

    public static List<Account> searchAccountByName(String searchTerm) {
        List<Account> accounts = [
            SELECT Id, Name, BillingCity, BillingState, Physical_Address__c, Physical_GeoLocation__c, Physical_Geolocation__latitude__s, Physical_Geolocation__longitude__s
            FROM Account
            WHERE Name Like :searchTerm
            ORDER BY Name
            LIMIT 10
        ];
        return accounts;
    }

    public static List<sked__Location__c> searchLocationByName(String searchTerm, String regionId) {
        if (String.isBlank(regionId)) {
            return new List<sked__Location__c>();
        }

        List<sked__Location__c> locations = [
            SELECT Id, Name, sked__Region__c, sked__Address__c, sked__GeoLocation__c, sked__GeoLocation__latitude__s, sked__GeoLocation__longitude__s
            FROM sked__Location__c  
            WHERE Name Like :searchTerm
            AND sked__Region__c = :regionId
            ORDER BY Name
            LIMIT 10
        ];
        return locations;
    }

    public static String buildLikeSearchString(String term) {
        if (String.isBlank(term)) {
            return '%%';
        }

        return '%' + String.escapeSingleQuotes(term).trim() + '%';
    }
//------------------------------Enrite Service Related Section------------------------------------------------------------------------------------------------------------------------------------------------------

    
}