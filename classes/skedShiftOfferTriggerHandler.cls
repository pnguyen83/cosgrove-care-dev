public class skedShiftOfferTriggerHandler {
    static public void onBeforeUpdate( List<Shift_Offer__c> newRecs, Map<Id, Shift_Offer__c> mpOldRecs ) {
        Set<Id> jobIds = new Set<Id>();
        Set<Id> resourceIds = new Set<Id>();
		List<Shift_Offer__c> acceptedShiftOffers = getAcceptedShiftOffers(newRecs, mpOldRecs);
		for ( Shift_Offer__c so : acceptedShiftOffers) {
            jobIds.add(so.Job__c);
            resourceIds.add(so.Resource__c);
        }
        Map<Id, sked__Job__c> mapJob = getMapJob(jobIds);
        Map<Id, sked__Resource__c> mapResource = getMapResource(resourceIds);
        for ( Shift_Offer__c so : acceptedShiftOffers) {
            sked__Resource__c resource = mapResource.get(so.Resource__c);
            sked__Job__c job = mapJob.get(so.Job__c);
            if ( isConflict(job, resource) ) {
                so.Status__c = 'Cancelled';
                continue;
            }
            so.Date_Time_Accepted__c = System.now();
        }
    }
    
    static public void onAfterUpdate( List<Shift_Offer__c> newRecs, Map<Id, Shift_Offer__c> mpOldRecs ) {
        List<Shift_Offer__c> acceptedShiftOffers = getAcceptedShiftOffers(newRecs, mpOldRecs);
        if ( acceptedShiftOffers.size() >0 ) 
            handleAcceptedOffers(acceptedShiftOffers);
    }
    
    static public void handleAcceptedOffers(List<Shift_Offer__c> acceptedShiftOffers) {
        Set<Id> jobIds = new Set<Id>();
        Set<Id> resourceIds = new Set<Id>();
		List<sked__Job_Allocation__c> jobAlls = new List<sked__Job_Allocation__c>();
        List<sked__Job__c> jobs = new List<sked__Job__c>();
        
        for ( Shift_Offer__c so : acceptedShiftOffers) {
            jobIds.add(so.Job__c);
            resourceIds.add(so.Resource__c);
        }
        Map<Id, sked__Job__c> mapJob = getMapJob(jobIds);
        Map<Id, sked__Resource__c> mapResource = getMapResource(resourceIds);
        Set<String> setAcceptedOffer = new Set<String>();
        
        for ( Shift_Offer__c so : acceptedShiftOffers) {
            sked__Resource__c resource = mapResource.get(so.Resource__c);
            sked__Job__c job = mapJob.get(so.Job__c);
            System.debug('isConflict#' + isConflict(job, resource));
            if ( !isConflict(job, resource) ) {
                sked__Job_Allocation__c jobAll = new sked__Job_Allocation__c();
                jobAll.sked__Resource__c = resource.Id;
                jobAll.sked__Job__c = job.Id;
                jobAll.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED;                
                jobAlls.add(jobAll);
                setAcceptedOffer.add(so.id);
            }
            if ( isConflict(job, resource) ) {
                //job.Skedulo_API_Error__c = true;
                //job.Skedulo_API_Error_Message__c = SkeduloConstants.SHIFT_OFFER_DECLINE_MSG;
                //jobs.add(job);
                continue;
            }
        }
        if ( jobAlls.size() >0 ) { 
            insert jobAlls;
        }
        if ( jobs.size() >0 ) {
            update jobs;
        }

        if (!setAcceptedOffer.isEmpty()) {
            updateAcceptedOfferStatus(setAcceptedOffer);
        }
    }

    @future
    public static void updateAcceptedOfferStatus(Set<String> setOfferId) {
        List<Shift_Offer__c> lstOffers = new List<Shift_Offer__c>();
        for (String offerId : setOfferId) {
            Shift_Offer__c offer = new Shift_Offer__c(id = offerId, Status__c = skedConstants.OFFER_ALLOCATED);
            lstOffers.add(offer);
        }

        update lstOffers;

        Set<String> setJobId = new Set<String>();
        Map<String, Shift_Offer__c> mapOffers = new Map<String, Shift_Offer__c>();

        for (Shift_Offer__c offer : [SELECT Id, Job__c FROM Shift_Offer__c WHERE Id IN : setOfferId]) {
            setJobId.add(offer.Job__c);
        }

        for (sked__Job__c job : [SELECT id, sked_No_of_Required_Resource__c, 
                                            (SELECT Id FROM Shift_Offers__r WHERE Status__c = :skedConstants.OFFER_OFFFERED),
                                            (SELECT Id FROM sked__Job_Allocations__r 
                                                WHERE sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED
                                                AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DELETED)
                                    FROM sked__Job__c 
                                    WHERE Id IN : setJobId]) {
            if (job.sked_No_of_Required_Resource__c == job.sked__Job_Allocations__r.size()) {
                for (Shift_Offer__c offer : job.Shift_Offers__r) {
                    offer.Status__c = skedConstants.OFFER_CANCELLED;
                    mapOffers.put(offer.id, offer);
                }
            }
        }

        if (!mapOffers.isEmpty()) {
            update mapOffers.values();
        }
    }
    
    /*==========================================================Private Method==================================================*/
    static private Boolean isConflict(sked__Job__c job, sked__Resource__c resource) {
        Boolean isConflict = false;
        for ( sked__Job_Allocation__c jobAll : resource.sked__Job_Allocations__r ) {
            if ( jobAll.sked__Job__r.sked__Start__c >= job.sked__Start__c &&  jobAll.sked__Job__r.sked__Start__c.addMinutes(1) <= job.sked__Finish__c 
                || jobAll.sked__Job__r.sked__Finish__c >= job.sked__Start__c.addMinutes(1) && jobAll.sked__Job__r.sked__Finish__c <= job.sked__Start__c
                || jobAll.sked__Job__r.sked__Start__c <= job.sked__Start__c && jobAll.sked__Job__r.sked__Finish__c >= job.sked__Finish__c ) {
                    isConflict = true;
                }                
        }
        return isConflict;
    }
    
    static private Map<Id, sked__Job__c> getMapJob(Set<Id> jobIds) {
        Map<Id, sked__Job__c> mapJob = new Map<Id, sked__Job__c>( [SELECT Id, 
                                                                   sked__Finish__c, 
                                                                   sked__Start__c                                                                   
                                                                   //Skedulo_API_Error__c,
                                                                   //Skedulo_API_Error_Message__c
                                                                   FROM sked__Job__c WHERE Id IN :jobIds] );
        return mapJob;
    }
    
    static private Map<Id, sked__Resource__c> getMapResource(Set<Id> resourceIds) {
        Map<Id, sked__Resource__c> mapResource = new Map<Id, sked__Resource__c>([SELECT Id,
                                                                                 name,
                                                                                 (SELECT 
                                                                                  Id, 
                                                                                  sked__Job__r.sked__Start__c, 
                                                                                  sked__Resource__c, 
                                                                                  sked__Job__r.sked__Finish__c                                                                                         
                                                                                  FROM sked__Job_Allocations__r
                                                                                  WHERE sked__Status__c != 'Declined' 
                                                                                  AND sked__Status__c != 'Deleted'
                                                                                 )
                                                                                 FROM sked__Resource__c 
                                                                                 WHERE Id IN :resourceIds]);
        return mapResource;
    }
    
    private static List<Shift_Offer__c> getAcceptedShiftOffers(List<Shift_Offer__c> newRecs, Map<Id, Shift_Offer__c> mpOldRecs) {
        List<Shift_Offer__c> acceptedShiftOffers = new List<Shift_Offer__c>();        
        for ( Shift_Offer__c newRec : newRecs ) {
            Shift_Offer__c oldRec = mpOldRecs.get(newRec.Id);
            if ( newRec.Status__c == 'Accepted' && oldRec.Status__c != newRec.Status__c ) {
                acceptedShiftOffers.add(newRec);
            }
        }
        return acceptedShiftOffers;
    }
}