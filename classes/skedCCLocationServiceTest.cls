@isTest
public class skedCCLocationServiceTest {
	testMethod static void getAPI_KEY_Test() {
		skedCCDataSetUp.setupCustomSettings();
		Test.startTest();
		String key = skedCCLocationServices.API_KEY;
		Test.stopTest();
	}

	static testmethod void tetGeocodingService() {
        
        insert new skedConfigs__c(Name='Google_API_KEY', Value__c='AIzaSyDG72vVxBIfBXO91BecFK7HLzCto8HuRBs');

        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));
        //Test insert
        sked__Location__c loc = new sked__Location__c(Name='Location', sked__Address__c='test address');
        insert loc;
        //Test update
        loc.sked__Address__c = 'another address';
        update loc;
        //Test batch
        skedCCBatchProcessor batch = new skedCCBatchProcessor(loc.Id, skedCCLocationServices.OBJ_LOCATION);
        Database.executeBatch(batch,1);
        
        Test.stopTest();
    }
}