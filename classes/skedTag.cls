public class skedTag {
	public string relatedId;
	public string name;
	public string id;
	public boolean required;
	public integer weighting;
	public skedTag(sked__Tag__c tag) {
		this.id = tag.Id;
		this.name = tag.Name;
	}

	public skedTag(sked__Job_Tag__c jobTag) {
		this.relatedId = jobTag.sked__Job__c;
		this.id = jobTag.sked__Tag__c;
		this.name = jobTag.sked__Tag__r.Name;
		this.required = jobTag.sked__Required__c;
		this.weighting = (Integer)(jobTag.sked__Weighting__c);
	}	
/*
	public skedTag(Client_Tag__c clientTag) {
		this.relatedId = clientTag.Client__c;
		this.id = clientTag.Tag__c;
		this.name = clientTag.Tag__r.Name;
		this.required = clientTag.Required__c;
		this.weighting = (Integer)(clientTag.Weighting__c);
	}

	public skedTag(Group_Tag__c groupTag) {
		this.relatedId = groupTag.Group_Event__c;
		this.id = groupTag.Tag__c;
		this.name = groupTag.Tag__r.Name;
		this.required = groupTag.Required__c;
		this.weighting = (Integer)(groupTag.Weighting__c);
	} */
}