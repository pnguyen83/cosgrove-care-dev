@isTest
public class skedAcceptOfferCtrlTest {
	testmethod static void fetchShiftOffersTest() {
		Map<string, sObject> mapTestData = skedCCDataSetup.setupCommonTestData();
		sked__Resource__c res1 = (sked__Resource__c)mapTestData.get('resourceCustomerService1');
		sked__Resource__c res2 = (sked__Resource__c)mapTestData.get('resourceCustomerService2');
		sked__Resource__c res3 = (sked__Resource__c)mapTestData.get('resourceCustomerService3');
		Contact ct = (Contact)mapTestData.get('contactTest');

		DateTime dt = system.now();
        sked__Region__c region = (sked__Region__c)mapTestData.get('regionQld');
        sked__Job__c job = new sked__Job__c(
            sked__Type__c = 'Desktop',
            sked__Region__c = region.Id,
            sked__Start__c = dt,
            sked__Finish__c = dt.addHours(1),
            sked__Duration__c = 60,
            sked__Address__c = '79 mclachlan Fortitude valley',
            sked__Job_Status__c = 'Pending Dispatch',
            sked__Contact__c = ct.id,
            sked__Description__c = 'abc',
            sked_No_of_Required_Resource__c = 2
        );
        insert job;

        Shift_Offer__c offer1 = new Shift_Offer__c(
        	Status__c = skedConstants.OFFER_OFFFERED,
        	Resource__c = res1.id,
        	Job__c = job.id
        );

        Shift_Offer__c offer2 = new Shift_Offer__c(
        	Status__c = skedConstants.OFFER_OFFFERED,
        	Resource__c = res2.id,
        	Job__c = job.id
        );

        sked__Job_Allocation__c ja = new sked__Job_Allocation__c(
        	sked__Job__c = job.id,
        	sked__Resource__c = res3.id,
        	sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_DISPATCHED
        );

        insert new List<sObject>{offer1, offer2, ja};

        test.startTest();
        skedAcceptOfferCtrl.fetchShiftOffers(res1.id);
        
        skedCCSkeduloApiManager.ApiResponse response = new skedCCSkeduloApiManager.ApiResponse();
        response.success = true;
        response.errorMessage = 'Error happens';
        response.errorCode  = '400';

        skedCCSkeduloApiManager.NotificationResponse response1 = new skedCCSkeduloApiManager.NotificationResponse();
        response1.jobId = job.id;

        skedCCSkeduloApiManager.NotificationError error1 = new skedCCSkeduloApiManager.NotificationError();
        error1.errorType = 'type 1';
        error1.message = 'Error happens';

        skedCCSkeduloApiManager.NotificationResponseResult notify = new skedCCSkeduloApiManager.NotificationResponseResult();
        notify.resourceId = res1.id;
        notify.protocol = 'GET';
        notify.error = error1;
        test.stopTest();
	}

	testmethod static void fetchShiftOffersTest2() {
		Map<string, sObject> mapTestData = skedCCDataSetup.setupCommonTestData();
		sked__Resource__c res1 = (sked__Resource__c)mapTestData.get('resourceCustomerService1');
		sked__Resource__c res2 = (sked__Resource__c)mapTestData.get('resourceCustomerService2');
		sked__Resource__c res3 = (sked__Resource__c)mapTestData.get('resourceCustomerService3');
		Contact ct = (Contact)mapTestData.get('contactTest');

		DateTime dt = system.now();
        sked__Region__c region = (sked__Region__c)mapTestData.get('regionQld');
        sked__Job__c job = new sked__Job__c(
            sked__Type__c = 'Desktop',
            sked__Region__c = region.Id,
            sked__Start__c = dt,
            sked__Finish__c = dt.addHours(1),
            sked__Duration__c = 60,
            sked__Address__c = '79 mclachlan Fortitude valley',
            sked__Job_Status__c = 'Pending Dispatch',
            sked__Contact__c = ct.id,
            sked__Description__c = 'abc',
            sked_No_of_Required_Resource__c = 1
        );
        insert job;

        Shift_Offer__c offer1 = new Shift_Offer__c(
        	Status__c = skedConstants.OFFER_OFFFERED,
        	Resource__c = res1.id,
        	Job__c = job.id
        );

        Shift_Offer__c offer2 = new Shift_Offer__c(
        	Status__c = skedConstants.OFFER_OFFFERED,
        	Resource__c = res2.id,
        	Job__c = job.id
        );

        sked__Job_Allocation__c ja = new sked__Job_Allocation__c(
        	sked__Job__c = job.id,
        	sked__Resource__c = res3.id,
        	sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_DISPATCHED
        );

        insert new List<sObject>{offer1, offer2, ja};

        test.startTest();
        skedAcceptOfferCtrl.fetchShiftOffers(res1.id);
        test.stopTest();
	}

	testmethod static void updateShiftOfferStatusTest() {
		Map<string, sObject> mapTestData = skedCCDataSetup.setupCommonTestData();
		sked__Resource__c res1 = (sked__Resource__c)mapTestData.get('resourceCustomerService1');
		sked__Resource__c res2 = (sked__Resource__c)mapTestData.get('resourceCustomerService2');
		sked__Resource__c res3 = (sked__Resource__c)mapTestData.get('resourceCustomerService3');
		Contact ct = (Contact)mapTestData.get('contactTest');

		DateTime dt = system.now();
        sked__Region__c region = (sked__Region__c)mapTestData.get('regionQld');
        sked__Job__c job = new sked__Job__c(
            sked__Type__c = 'Desktop',
            sked__Region__c = region.Id,
            sked__Start__c = dt,
            sked__Finish__c = dt.addHours(1),
            sked__Duration__c = 60,
            sked__Address__c = '79 mclachlan Fortitude valley',
            sked__Job_Status__c = 'Pending Dispatch',
            sked__Contact__c = ct.id,
            sked__Description__c = 'abc',
            sked_No_of_Required_Resource__c = 2
        );
        insert job;

        Shift_Offer__c offer1 = new Shift_Offer__c(
        	Status__c = skedConstants.JOB_ALLOCATION_STATUS_DECLINED,
        	Resource__c = res1.id,
        	Job__c = job.id
        );

        insert offer1;

        test.startTest();
        skedAcceptOfferCtrl.updateShiftOfferStatus(offer1.id, skedConstants.OFFER_DECLINED);
        test.stopTest();
	}

	testmethod static void updateShiftOfferStatusTest2() {
		Map<string, sObject> mapTestData = skedCCDataSetup.setupCommonTestData();
		sked__Resource__c res1 = (sked__Resource__c)mapTestData.get('resourceCustomerService1');
		sked__Resource__c res2 = (sked__Resource__c)mapTestData.get('resourceCustomerService2');
		sked__Resource__c res3 = (sked__Resource__c)mapTestData.get('resourceCustomerService3');
		Contact ct = (Contact)mapTestData.get('contactTest');

		DateTime dt = system.now();
        sked__Region__c region = (sked__Region__c)mapTestData.get('regionQld');
        sked__Job__c job = new sked__Job__c(
            sked__Type__c = 'Desktop',
            sked__Region__c = region.Id,
            sked__Start__c = dt,
            sked__Finish__c = dt.addHours(1),
            sked__Duration__c = 60,
            sked__Address__c = '79 mclachlan Fortitude valley',
            sked__Job_Status__c = 'Pending Dispatch',
            sked__Contact__c = ct.id,
            sked__Description__c = 'abc',
            sked_No_of_Required_Resource__c = 2
        );
        insert job;

        Shift_Offer__c offer1 = new Shift_Offer__c(
        	Status__c = skedConstants.OFFER_CANCELLED,
        	Resource__c = res1.id,
        	Job__c = job.id
        );

        insert offer1;

        test.startTest();
        skedAcceptOfferCtrl.updateShiftOfferStatus(offer1.id, skedConstants.OFFER_CANCELLED);
        test.stopTest();
	}

	testmethod static void updateShiftOfferStatusTest3() {
		Map<string, sObject> mapTestData = skedCCDataSetup.setupCommonTestData();
		sked__Resource__c res1 = (sked__Resource__c)mapTestData.get('resourceCustomerService1');
		sked__Resource__c res2 = (sked__Resource__c)mapTestData.get('resourceCustomerService2');
		sked__Resource__c res3 = (sked__Resource__c)mapTestData.get('resourceCustomerService3');
		Contact ct = (Contact)mapTestData.get('contactTest');

		DateTime dt = system.now();
        sked__Region__c region = (sked__Region__c)mapTestData.get('regionQld');
        sked__Job__c job = new sked__Job__c(
            sked__Type__c = 'Desktop',
            sked__Region__c = region.Id,
            sked__Start__c = dt,
            sked__Finish__c = dt.addHours(1),
            sked__Duration__c = 60,
            sked__Address__c = '79 mclachlan Fortitude valley',
            sked__Job_Status__c = 'Pending Dispatch',
            sked__Contact__c = ct.id,
            sked__Description__c = 'abc',
            sked_No_of_Required_Resource__c = 2,
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080
        );
        insert job;

        Shift_Offer__c offer1 = new Shift_Offer__c(
        	Status__c = skedConstants.OFFER_OFFFERED,
        	Resource__c = res1.id,
        	Job__c = job.id
        );

        insert offer1;

        test.startTest();
        skedAcceptOfferCtrl.updateShiftOfferStatus(offer1.id, skedConstants.OFFER_ACCEPT);
        test.stopTest();
	}

	testmethod static void fetchResourceTest() {
		Map<string, sObject> mapTestData = skedCCDataSetup.setupCommonTestData();
		sked__Resource__c res1 = (sked__Resource__c)mapTestData.get('resourceCustomerService1');
		String userId = UserInfo.getUserId();

		DateTime dt = system.now();
        sked__Region__c region = (sked__Region__c)mapTestData.get('regionQld');
        sked__Job__c job = new sked__Job__c(
            sked__Type__c = 'Desktop',
            sked__Region__c = region.Id,
            sked__Start__c = dt,
            sked__Finish__c = dt.addHours(1),
            sked__Duration__c = 60,
            sked__Address__c = '79 mclachlan Fortitude valley',
            sked__Job_Status__c = 'Pending Dispatch',
            
            sked__Description__c = 'abc',
            sked_No_of_Required_Resource__c = 2,
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080
        );
        insert job;

        Shift_Offer__c offer1 = new Shift_Offer__c(
        	Status__c = skedConstants.OFFER_OFFFERED,
        	Resource__c = res1.id,
        	Job__c = job.id
        );

        insert offer1;

		test.startTest();
		skedAcceptOfferCtrl.fetchResource(userId);
		skedAcceptOfferCtrl.Offer offer = new skedAcceptOfferCtrl.Offer(offer1);
		test.stopTest();
	}
}