@isTest
public class skedCCJobSchedulingCtrl_Test {
    @isTest static void test_getAvailableResources() {
        Test.startTest();
        
        List<skedCCResourceAvailabilityBase.eventModel> eventModels = new List<skedCCResourceAvailabilityBase.eventModel>();
        DateTime curTime = System.now();

        for (Integer i = 0; i < 5; i++) {
            skedCCResourceAvailabilityBase.eventModel eventModel = new skedCCResourceAvailabilityBase.eventModel();
            eventModel.start = curTime.addHours(i);
            eventModel.finish = curTime.addHours(i+1);
            eventModels.add(eventModel);
        }
        eventModels[0].geoLocation = null;
        eventModels[0].eventDetails = '';
        eventModels.sort();

        Test.stopTest();
    }

    public static skedCCJobSchedulingModels.ResourceCostingRequestModel getResourceCostingModel(skedCCJobSchedulingModels.JobModel jobModel) {
        skedCCJobSchedulingModels.ResourceCostingRequestModel costingRequest = new skedCCJobSchedulingModels.ResourceCostingRequestModel();
        costingRequest.startDate = jobModel.startDate;
        costingRequest.startTime = jobModel.startTime;
        costingRequest.duration = jobModel.duration;
        costingRequest.role = jobModel.resourceRequirements[0].role;
        costingRequest.timeZone = jobModel.region.timeZone;
        costingRequest.resourceIds = new List<String>();
        return costingRequest;
    }

    public static skedCCJobSchedulingModels.JobModel getNewJobModel(Map<string, sObject> mapTestData, Map<String, sObject> mapEnrTestData) {
        skedCCJobSchedulingModels.JobModel job = new skedCCJobSchedulingModels.JobModel();

        DateTime curDT = system.now().addDays(7);
        String curDTISO = curDT.format('yyyy-MM-dd');
        sked__Region__c qldReg = (sked__Region__c)mapTestData.get('regionQld');
        Account accTest = (Account)mapTestData.get('accountTest');

        if (mapEnrTestData != null) {
            job.contactId = mapEnrTestData.get('client').Id;
        }

        job.startDate = curDTISO;
        job.startTime = 900;
        job.duration = 60;
        job.urgency = 'Normal';

        job.region = new skedCCModels.RegionModel(qldReg);
        job.addressType = 'Account';
        job.account = new skedCCJobSchedulingModels.AccountModel(accTest.Id, accTest.Name);

        job.address = new skedCCModels.JobLocation();
        job.address.fullAddress = '79 McLachlan St, Fortitude Valley, QLD, 4006, Australia';
        job.address.latitude = -27.457730;
        job.address.longitude = 153.037080;

        job.description = 'Test';
        job.unscheduled = false;

        job.resourceRequirements = new List<skedCCJobSchedulingModels.ResourceRequirementModel>();

        skedCCJobSchedulingModels.ResourceRequirementModel requirement = new skedCCJobSchedulingModels.ResourceRequirementModel();
        requirement.role = mapTestData.get('eeGroupCarer').Id;
        requirement.noOfResources = 1;
        requirement.asset = 1;
        requirement.tags.add(mapTestData.get('tagServerEngineer').Id);
        job.resourceRequirements.add(requirement);

        if (mapEnrTestData != null) {
            String categoryId = mapEnrTestData.get('category').Id;
            String serviceId = mapEnrTestData.get('service').Id;
            String siteId = mapEnrTestData.get('site').Id;
            String rateId = mapEnrTestData.get('rate').Id;
            String saiId = mapEnrTestData.get('serviceAgreementItem').Id;

            job.services = new List<skedCCJobSchedulingModels.JobServiceItemModel>();
            skedCCJobSchedulingModels.JobServiceItemModel sim = new skedCCJobSchedulingModels.JobServiceItemModel();
            sim.deliveryMethod = 'Scheduled Time';
            sim.category = new skedCCJobSchedulingModels.CategoryBaseModelExt(categoryId, categoryId);
            sim.service = new skedCCJobSchedulingModels.CategoryBaseModel(serviceId, serviceId);
            sim.site = new skedCCJobSchedulingModels.CategoryBaseModel(siteId, siteId);
            sim.rate = new skedCCJobSchedulingModels.CategoryBaseModel(rateId, rateId);
            sim.serviceAgreementItem = new skedCCJobSchedulingModels.CategoryBaseModel(saiId, saiId);
            job.services.add(sim);
        }

        return job;
    }
}