@istest
public class skedDataFactory {

    //Create List of Regions
    public static list<sked__Region__c> createRegions(){
        skedAddressOption opt = new skedAddressOption();
        list<sked__Region__c> lstReg = new list<sked__Region__c>();
        sked__Region__c reg = new sked__Region__c();
        reg.Name = 'Western Australia';
        reg.sked__Timezone__c = 'Australia/Sydney';
        reg.sked__Country_Code__c = 'AU';

        lstReg.add(reg);

        sked__Region__c reg1= new sked__Region__c();
        reg1.Name = 'Melbourne';
        reg1.sked__Timezone__c = 'Australia/Sydney';
        reg1.sked__Country_Code__c = 'AU';

        lstReg.add(reg1);

        insert lstReg;
        return lstReg;
    }

    public static Group_Event__c createGroupEvent(Contact emp,sked__Region__c reg,Account acc, Contact client){
        Group_Event__c GE = new Group_Event__c();
        GE.Name = 'Test Group';
        GE.Group_Status__c = 'Active';
        GE.Coordinator__c = emp.id;
        GE.Address__c = 'Test Address';
        GE.Region__c = reg.id;
        GE.Account__c = acc.id;

        insert GE;

        Group_Client__c GC = new Group_Client__c();
        GC.Group_Event__c = GE.id;
        GC.Client__c = client.id;
        
        insert GC;

        return GE;

    }

    public static Account createAccount(){
        //Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household Account').getRecordTypeId();
        Account acc = new Account();
        //acc.RecordTypeId = devRecordTypeId;
        acc.Name = 'Test Household';
        acc.ShippingStreet = 'Test';
        acc.ShippingStreet = 'Test';
        acc.ShippingStreet = 'Test';
        acc.ShippingStreet = 'Test';
        acc.ShippingStreet = 'Test';
        insert acc;

        return acc;

    }

    public static Account createOganization(){
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Organization').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = devRecordTypeId;
        acc.Name = 'Test Organization';
        acc.ShippingStreet = 'Test';
        acc.ShippingPostalCode = 'Test';
        acc.ShippingState = 'Test';
        acc.ShippingCity = 'Test';
        acc.ShippingCountry = 'Test';
        insert acc;

        return acc;

    }

    public static Contact createEmployee(Account acc){
        Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();

        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Employee';
        con.AccountId = acc.id;
        con.RecordTypeId = devRecordTypeId;
        con.OtherCity = 'NY';
        con.OtherPostalCode = '4006';
        con.Phone = '0401467938';

        insert con;

        return con;
    }


    public static Contact createClient(Account acc, string Name){
        Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();

        Contact con = new Contact();
        con.FirstName = Name;
        con.LastName = Name;
        con.AccountId = acc.id;
        con.RecordTypeId = devRecordTypeId;
        con.Birthdate = System.today().addDays(-10000);
        con.otherStreet = 'Test';
        con.otherCity = 'Test';
        con.otherState = 'Test';
        con.otherPostalCode = 'Test';
        con.otherCountry = 'Test';
        con.Phone = '1234567890';
        con.Email = 'test@test.com';
        insert con;

        return con;
    }

    

    public static Map<String, RecordType> RecordTypes
    {
        get
        {
            if (RecordTypes == null)
            {
                RecordTypes = new Map<String, RecordType>();

                for (RecordType rt : [SELECT Id,
                        sObjectType,
                        DeveloperName
                FROM RecordType])
                {
                    RecordTypes.put(rt.sObjectType + ':' + rt.DeveloperName, rt);
                }
            }
            return RecordTypes;
        }
        set;
    }

    public static sked__Location__c createLocation(){
        sked__Location__c loc = new sked__Location__c();
        loc.Name = 'Test Location';
        loc.sked__Address__c = 'Test Location';
        insert loc;
        return loc;
    }
}