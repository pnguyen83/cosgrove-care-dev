@isTest
public class skedCCConfigsTest {
	testmethod static void configTest() {
		skedCCDataSetUp.setupCustomSettings();
		Test.startTest();
		String jobURL = skedCCConfigs.JOB_URL;
		Integer defaultJobDuration = skedCCConfigs.DEFAULT_JOB_DURATION;
		String defaultDeliveryMethod = skedCCConfigs.DEFAULT_DELIVERY_METHOD;
		String apiTOken = skedCCConfigs.SKEDULO_API_TOKEN; 
		
		Test.stopTest();
	}

	testmethod static void configTest2() {
		Test.startTest();
		String jobURL = skedCCConfigs.JOB_URL;
		Integer defaultJobDuration = skedCCConfigs.DEFAULT_JOB_DURATION;
		String defaultDeliveryMethod = skedCCConfigs.DEFAULT_DELIVERY_METHOD;
		String apiTOken = skedCCConfigs.SKEDULO_API_TOKEN; 
		Test.stopTest();
	}

	testmethod static void skedCCConstantsTest() {
		skedCCDataSetUp.setupCustomSettings();
		Test.startTest();
		String current = skedCCConstants.STATUS_CURRENT;
		String avaiApproved = skedCCConstants.AVAILABILITY_STATUS_APPROVED;
		String avaiPending = skedCCConstants.AVAILABILITY_STATUS_PENDING;
		String contentURL = skedCCConstants.SF_CONTENTVERSION_URL;
		String addresCountry = skedCCConstants.ADDRESS_COUNTRY;
		String addressType = skedCCConstants.ADDRESS_TYPE;
		Test.stopTest();
	}

	testmethod static void skedCCConstantsTest2() {
		Test.startTest();
		String current = skedCCConstants.STATUS_CURRENT;
		String avaiApproved = skedCCConstants.AVAILABILITY_STATUS_APPROVED;
		String avaiPending = skedCCConstants.AVAILABILITY_STATUS_PENDING;
		String contentURL = skedCCConstants.SF_CONTENTVERSION_URL;
		String addresCountry = skedCCConstants.ADDRESS_COUNTRY;
		Test.stopTest();
	}
}