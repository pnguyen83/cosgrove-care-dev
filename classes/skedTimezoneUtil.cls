/*	
	========= Format ========
	'YYYY-MM-dd HH:mm:ss'
	'America/Los_Angeles'

 	Timezone rule.
 	1.Create a  startDateTime in Destination Timezone.
	2.Convert to GMT
	3.Calculate business logic in GMT 
	4. format as Destination timezone(Text), and return to client.	
*/

public class skedTimezoneUtil {
	/*
		Returns the time zone offset, in minutes, of the specified date(selectedDT) to the GMT time zone.
		#will fix issue for daylight saving time.
		# Offset between Userlogin timezone and GMT. 
		Example: if userlogin in Los Angeles. result will be -4h or -5h. 
	*/

	public static integer getOffsetInMinute(Datetime selectedDT){
		TimeZone tz = UserInfo.getTimeZone(); 
		return tz.getOffset(selectedDT)/1000/60;
	}


	/*
		Example:
		Timezone tz = Timezone.getTimeZone('America/New_York');
		DateTime dtpre = DateTime.newInstanceGMT(2000, 11, 1, 0, 0, 0); // (= -5 hours = EST)

		if
		DateTime dtpost = DateTime.newInstanceGMT(2012, 11, 1, 0, 0, 0); //(= -4 hours = EDT)

	*/
	public static integer getOffsetInMinute(string inputTimezone, Datetime selectedDT){
		TimeZone tz = Timezone.getTimeZone(inputTimezone);
		return tz.getOffset(selectedDT)/1000/60;

	}

	/*
	Example:
		getOffset('America/New_York', 'America/Los_Angeles', Datetime.now()) 
		return: 180 
	*/
	public static integer getOffsetInMinute(string fromTimezone, string toTimezone, Datetime selectedDT){
		integer diff = getOffsetInMinute(fromTimezone, selectedDT) - getOffsetInMinute(toTimezone, selectedDT);
		return diff;
	}

	public static string DATE_ISO_FORMAT = '"yyyy-MM-dd"';

	public static Date ConvertToDateValue(string dateString) {
        String[] temp = dateString.split('-');
        return Date.newInstance(Integer.valueOf(temp[0]), Integer.valueOf(temp[1]), Integer.valueOf(temp[2]));
    }

    // @param inputTime: 730
    // @result: 730 will be considered as 7 hour and 30 minutes so the result should be 7 * 60 + 30 = 450
    public static integer ConvertTimeNumberToMinutes(integer inputTime) {
        return integer.valueOf(inputTime / 100) * 60 + Math.mod(inputTime, 100);
    }
    /*
    public static string ConvertDateToIsoString(Date input) {
        string result = Json.serialize(input).replace('"', '');
        return result;
    }
	*/

	/*
	Return Skedulo time format.
	example: 9:00 AM --> 900, 9:30 AM --> 930.
		Used for convert from Inspector Timezone to CurrentLogin timezone.
	*/
	public static integer convertTimezone(string fromTimezone, string toTimezone, Datetime selectedDT, integer inputTimeInt ){
		integer total = 0;
		integer result = 0;
		
		//if(string.isBlank(fromTimezone) || string.isBlank(toTimezone) ){
			//throw new skedException('Cannot convert Timezone, it could be no user link to resource.');
		//}
		integer diff = -(getOffsetInMinute(fromTimezone, selectedDT) - getOffsetInMinute(toTimezone, selectedDT));
		integer diffFormat = (diff/60)*100 + Math.mod(diff,60);//400
		total =  inputTimeInt + diffFormat;//930 + 400 = 1330		
		result = Math.mod(total,100);//70
		result =   (result<60)?result:(100+Math.mod(result,60));//70 -->110.	

		return (total/100)*100 + result;
	}

	

	/*
		#fromTimezone: example: Asia/Ho_Chi_Minh
		#dateTimeText: YYYY-MM-DD hh:mm:ss, example: 2016-04-21 08:00:00
	*/

	public static Datetime convertToGMT(string fromTimezone, string dateTimeText){

		integer year 	= Integer.valueOf(dateTimeText.substring(0,4));
		integer month 	= Integer.valueOf(dateTimeText.substring(5,7));
		integer datee 	= Integer.valueOf(dateTimeText.substring(8,10));
		integer hour 	= Integer.valueOf(dateTimeText.substring(11,13));
		integer minute 	= Integer.valueOf(dateTimeText.substring(14,16));
		integer second 	= Integer.valueOf(dateTimeText.substring(17,19));		

		Datetime dtSource = Datetime.newInstanceGmt(year, month, datee, hour, minute, second);
		system.debug('dtSource===' + dtSource);
		return convertToGMT(fromTimezone, dtSource);

	}
	
	//in-used.
	public static Datetime convertToGMT(string fromTimezone, DateTime basedOnDateGMT){
		Timezone frmTZ = Timezone.getTimeZone(fromTimezone);//America/New_York
		integer offsertToGMT = -frmTZ.getOffset(basedOnDateGMT)/1000/60;//DateTime.newInstanceGMT(2000, 11, 1, 0, 0, 0); --> +7hours
		system.debug('fromTimezone --> GMT, hours ===' + offsertToGMT/60);
		return basedOnDateGMT.addMinutes(offsertToGMT);

	}
	
	public static Datetime convertFromGMT(DateTime basedOnDateGMT, string toTimezone){
		Timezone frmTZ = Timezone.getTimeZone(toTimezone);
		integer offsertToGMT = frmTZ.getOffset(basedOnDateGMT)/1000/60;
		//system.debug('fromTimezone --> GMT, hours ===' + offsertToGMT/60);
		return basedOnDateGMT.addMinutes(offsertToGMT);
	}

	public static DateTime convertTimezone(Datetime fromDate, String fromTZ, String toTZ){
        Datetime gmtDate = convertToGMT(fromTZ, fromDate);
        return convertFromGMT(gmtDate, toTZ);
    }

    public static integer toMinute(integer startTime){
	    	return math.mod(startTime, 100) + Integer.valueOf(startTime / 100) * 60;
	    }

    //convert intege time(900,1300) to minute
    public static integer toTime(integer mins){
    	return math.mod(mins, 60) + Integer.valueOf(mins / 60) * 100;
    }
    //1430 -> 2:30pm
    public static String toTimeString(Integer startTime){
    	Integer minutes = toMinute(startTime);
    	DateTime dt = DateTime.newInstance(System.today(), Time.newInstance(0,0,0,0));
    	dt 			= dt.addMinutes(minutes);
    	return dt.format('hh:mm a');
    }

    //input formated: 9:30AM, or 12:30PM, 16:00PM (or 09:30 AM - 9:30 AM)
	//return: 930 or 1230
	//NOTE: In-case minutes only has 1 character, we need to modify this to 2 characters.

	//OR Datetime.format(HHmm)
	public static integer convertTimeString2Integer(string inputTime){
		boolean isAfterNoon = false;
		if(inputTime.indexOf('PM') > - 1 || inputTime.indexOf('pm') > -1){
			isAfterNoon = true;
		}
		integer hour 	= Integer.valueOf(inputTime.split(':')[0]);
		integer minute 	= Integer.valueOf(inputTime.split(':')[1].remove('AM').remove('am').remove('PM').remove('pm').trim());
		if(isAfterNoon && hour != 12) hour = hour + 12;

		if(isAfterNoon == false && hour == 12){
			//12:00am -->0, 12:30am -> 30.
			hour = 0;
		}

		return (hour*100 + minute);
	}

	//calculate minutes between two datetime
	public static integer minutesBetweenDateTimes(DateTime dtime1, DateTime dTime2) {
		Long startInSecond = dtime1.getTime()/1000; 
		Long finishInSecond = dTime2.getTime()/1000;
		Integer result = Integer.valueOf((finishInSecond - startInSecond)/60);

		return result;
	}

	public static integer minutesBetweenDateTimes1(DateTime dtime1, DateTime dTime2) {
		TimeZone timeZone = UserInfo.getTimeZone();
		Long beforeDSTOffset = timeZone.getOffset(dtime1);
		Long afterDSTOffset = timeZone.getOffset(dTime2);
		Long offsetMinutes = afterDSTOffset - beforeDSTOffset;
		offsetMinutes = offsetMinutes / 60000;
		System.debug('offsetMinutes==' + offsetMinutes);
		Long startInSecond = dtime1.getTime()/1000; 
		Long finishInSecond = dTime2.getTime()/1000;
		Integer result = Integer.valueOf( ((finishInSecond - startInSecond)/60) + offsetMinutes) ;

		return result;
	}

	//7:00AM in 'America/Chicago', login in Chicago user and want convert to 7:00AM in 'America/Los_Angeles'- login in Chicago user.
    //--> Chicago offset-5; Los_Angeles offset-7 --> (-7 +5)
    //@Result is 5:00AM in Chicago, but 7:00AM in Los_Angeles.
    public static DateTime ConvertBetweenTimezones(DateTime input, string fromTimezoneSidId, string toTimezoneSidId) {
        if (fromTimezoneSidId == toTimezoneSidId) {
            return input;
        }
        TimeZone fromTz = Timezone.getTimeZone(fromTimezoneSidId);
        Timezone toTz = Timezone.getTimeZone(toTimezoneSidId);
        integer offsetMinutes = toTz.getOffset(input) - fromTz.getOffset(input);
        offsetMinutes = offsetMinutes / 60000;
        System.debug('offsetMinutes==' + offsetMinutes);
        input = input.addMinutes(offsetMinutes);
        return input;
    }

    public static DateTime addMinutes(DateTime input, integer minutes, string timezoneSidId) {
        DateTime result = input.addMinutes(minutes);
        Timezone tz = Timezone.getTimezone(timezoneSidId);
        integer inputOffset = tz.getOffset(input) / 60000;
        integer resultOffset = tz.getOffset(result) / 60000;
        result = result.addMinutes(inputOffset - resultOffset);
        
        return result;
    }

    public static DateTime addDays(DateTime input, integer days, string timezoneSidId) {
        DateTime result = input.addDays(days);
        Timezone tz = Timezone.getTimezone(timezoneSidId);
        integer inputOffset = tz.getOffset(input) / 60000;
        integer resultOffset = tz.getOffset(result) / 60000;
        result = result.addMinutes(inputOffset - resultOffset);
        
        return result;
    }

}